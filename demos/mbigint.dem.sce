mode(1)
//
// Demo of mbigint.sci
//

//convert double matrix to mbigint
M=mbigint(grand(2,3,'uin',1,99999999))
typeof(M)  //  mbigint
x=mbigint(123456789)
typeof(x)  // simplification to bigint
// create a matrix of bigint
x=bigint(123456789)
y=bigint(987654321)
z=x-y
M=mbigint(2,2,x,y,z,x-z)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
