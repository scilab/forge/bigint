mode(1)
//
// Demo of %s_x_bigint.sci
//

halt()   // Press return to continue
 
x=12345678
y=bigint('98765432')
x*y  //  scalar/bigint product
12345678*98765432 // double product
x.*y // no type error
(x*ones(2,2)).*y  // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
