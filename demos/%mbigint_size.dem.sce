mode(1)
//
// Demo of %mbigint_size.sci
//

//
x=bigint('12345678')
y=bigint('-98765432')
M=[x y; x-y bigint(0)]
N=[M,M]
[p,n]=size(M) // = p=2,n=2
[p,n]=size(N) // = p=2,n=4
p=size(N,1) // = p=2,n=2
n=size(N,2) // = p=2,n=2
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
