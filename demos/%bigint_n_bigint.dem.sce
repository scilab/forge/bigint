mode(1)
//
// Demo of %bigint_n_bigint.sci
//

//
x=bigint('123456789')
y=bigint('0123456789')
z=bigint('1234567890')
x<>y  // false
x<>z  // true
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
