mode(1)
//
// Demo of %mbigint_i_mbigint.sci
//

halt()   // Press return to continue
 
x=bigint('123456789')
y=bigint('02345678')
M=[x y x-y bigint(0)]
M(2,2)=x//  change cell (2,2)
M(1,1:2)=y  // change line 1
M(1:2,2)=-x //change column 2
M(:,1)=y     // short cuts
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
