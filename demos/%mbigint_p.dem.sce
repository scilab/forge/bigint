mode(1)
//
// Demo of %mbigint_p.sci
//

halt()   // Press return to continue
 
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
T=cell();
T{1,1}=x;
T{1,2}=y;
T{2,1}=z;
T{2,2}=bigint(0);
M=mlist(['mbigint','display','value'],[string(x) string(y); string(z) '0' ],T)
typeof(M)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
