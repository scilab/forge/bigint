mode(1)
//
// Demo of %s_o_bigint.sci
//

halt()   // Press return to continue
 
x=123456789
y=bigint('123456789')
x==y  //  True
x=12345678
x==y // false
[123456789 12345678]==y // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
