mode(1)
//
// Demo of %mbigint_j_mbigint.sci
//

//
x=bigint('12345')
y=bigint('98765')
M=[x y; y x]
N=mbigint([0 1; 2 3])
M.^N
y^2
x^3
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
