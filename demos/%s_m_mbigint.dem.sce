mode(1)
//
// Demo of %s_m_mbigint.sci
//

//
x=123456
y=456789
z=987654
M=[x y; z x-z]
NN=[x -z; -y x-z]
N=mbigint(NN)
M*N
MM*NN //double verification
P=[x x;x x]
P*N  // matrix case
P*NN
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
