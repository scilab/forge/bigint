mode(1)
//
// Demo of %bigint_p_bigint.sci
//

halt()   // Press return to continue
 
format('v',20)
x=bigint('5')
y=bigint('7')
x^7
5^7  // compare with doubles
x=bigint('12345678')
y=bigint('2')
x^y
12345678^2
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
