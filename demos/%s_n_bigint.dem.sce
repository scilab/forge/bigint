mode(1)
//
// Demo of %s_n_bigint.sci
//

halt()   // Press return to continue
 
x=123456789
y=bigint('123456789')
x<>y  //  false
x=12345678
x<>y // true
[123456789 12345678]<>y // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
