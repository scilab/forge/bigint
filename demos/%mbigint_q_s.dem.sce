mode(1)
//
// Demo of %mbigint_q_s.sci
//

halt()   // Press return to continue
 
x=123456789
xx=bigint(x)
y=9876543
yy=bigint(y)
x*y
M=[x y; -x -x]
N=[yy xx; yy -yy]
N.\M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
