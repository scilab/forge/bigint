mode(1)
//
// Demo of %s_i_mbigint.sci
//

halt()   // Press return to continue
 
x=bigint('123456789')
y=bigint('02345678')
M=[x y;x-y bigint(0)]
M(2,2)=1    //  change cell (2,2)
M(1,1:2)=0
M(1:2,2)=-1
M(:,1)=2
M(1:2,2)=[]  // suppress 2nd column
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
