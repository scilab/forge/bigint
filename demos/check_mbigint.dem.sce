mode(1)
//
// Demo of check_mbigint.sci
//

halt()   // Press return to continue
 
x=bigint('1234567890')
y=bigint('9876543210')
z=x-y
T=cell(2,2)
T{1,1}=x;T{1,2}=y;
T{2,1}=z;T{2,2}=bigint(0)
D=[string(x) string(y);string(z),'0']
M=mlist(['mbigint','display','value'],D,T)
M=check_mbigint(M) // ok
M=mlist(['mbigint','display','value'],'0',T)
M=check_mbigint(M) // error
M=mlist(['mbigint','display','value'],D,x)
M=check_mbigint(M) // error
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
