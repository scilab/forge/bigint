mode(1)
//
// Demo of %bigint_m_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('9876543')
M=[y x; -x -y]
N=[x x;x x]
x*M
N*M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
