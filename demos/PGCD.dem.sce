mode(1)
//
// Demo of PGCD.sci
//

halt()   // Press return to continue
 
x=bigint('98765432')
y=bigint('12345678')
d=PGCD(x,y)
a=x/d
b=y/d
PGCD(a,b) // =1
x=bigint('123456789987654321')
y=bigint('987654321123456789')
PGCD(x,y)//= 1222222221
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
