mode(1)
//
// Demo of %bigint_x_bigint.sci
//

//
x=bigint('12345678')
y=bigint('98765432')
x*y  //  bigint product
12345678*98765432 // double product
x.*y //  no type error
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
