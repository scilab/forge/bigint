mode(1)
//
// Demo of %mbigint_length.sci
//

halt()   // Press return to continue
 
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
M=[x y z]
[p,n]=size(M) // = p=1,n=3
length(M) // = 3
M=[M; M]
[p,n]=size(M) // = p=2,n=3
length(M)  //= 6
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
