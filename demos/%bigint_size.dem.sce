mode(1)
//
// Demo of %bigint_size.sci
//

//
x=bigint('12345678')
[p,n]=size(x) // = p=1,n=1
p=size(x,1) // = p=1
n=size(x,2) // = n=1
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
