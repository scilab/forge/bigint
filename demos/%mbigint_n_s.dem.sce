mode(1)
//
// Demo of %mbigint_n_s.sci
//

//
x=bigint('123456789')
y=bigint('987654321')
A=[x y]
B=[y x]
C=[x x]
A<>123456789
B<>123456789
C<>123456789
A<>[123456789 987654321]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
