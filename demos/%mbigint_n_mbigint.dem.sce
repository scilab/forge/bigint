mode(1)
//
// Demo of %mbigint_n_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('0123456789')
A=[x y]
B=[y x]
C=[x x]
A<>A  // true
A<>B  // false
A<>C
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
