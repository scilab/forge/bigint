mode(1)
//
// Demo of %mbigint_sum.sci
//

halt()   // Press return to continue
 
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
L=[x y z]
sum(L) //= 2*x
2*x
M=[L; L]
sum(M)
sum(M,'r') // = [2*x 2*y 2*z]
sum(M,'c') // = [2*x; 2*x]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
