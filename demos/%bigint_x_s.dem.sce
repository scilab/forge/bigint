mode(1)
//
// Demo of %bigint_x_s.sci
//

//
x=bigint('12345678')
y=98765432
x*y  //  scalar/bigint product
12345678*98765432 // double product
x.*y  //  no type error
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
