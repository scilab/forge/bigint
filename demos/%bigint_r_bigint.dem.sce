mode(1)
//
// Demo of %bigint_r_bigint.sci
//

halt()   // Press return to continue
 
x=bigint('98765432')
y=bigint('12345678')
q=x/y  //  bigint division
98765432/12345678
r=x-q*y
pmodulo(98765432,12345678) // double product
y=bigint('1234567890')
x=bigint('987654321123456789')
q=x/y
r=x-q*y
x== q*y+r
r<abs(y)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
