mode(1)
//
// Demo of %s_q_bigint.sci
//

halt()   // Press return to continue
 
y=bigint('98765432')
x=12345678
q=x.\y  //  bigint division
98765432/12345678
r=y-q*x
pmodulo(98765432,12345678) //  division with doubles
y== q*x+r
r<abs(x)
(x*ones(2,2)).\y   // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
