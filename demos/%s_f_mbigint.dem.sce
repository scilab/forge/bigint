mode(1)
//
// Demo of %s_f_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('02345678')
L=[x;y]
M=[1;L]            // scalar case
L=[x y ; y x]
M= [zeros(2,2);L]  // matrix case
M=[ []; L]        // empty case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
