mode(1)
//
// Demo of odd.sci
//

halt()   // Press return to continue
 
x=bigint('12345678')
y=bigint('123456789')
odd(x) //=0
odd(y) //=1
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
