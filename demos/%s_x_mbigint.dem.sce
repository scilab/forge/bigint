mode(1)
//
// Demo of %s_x_mbigint.sci
//

//
x=123456
y=456789
z=987654
MM=[x y; z x-z]
M=mbigint(MM)
x.*M
x.*MM //double verification
P=[x x;x x]
P*N
bigint(x)*N
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
