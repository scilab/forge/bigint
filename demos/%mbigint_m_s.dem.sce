mode(1)
//
// Demo of %mbigint_m_s.sci
//

//
x=123456
y=456789
z=987654
MM=[x y; z x-z]
M=mbigint(MM)
NN=[x -z; -y x-z]
N=mbigint(NN)
M*NN
MM*NN //double verification
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
