mode(1)
//
// Demo of %mbigint_n_bigint.sci
//

//
x=bigint('123456789')
y=bigint('987654321')
A=[x y]
B=[y x]
C=[x x]
A<>x
B<>x
C<>x
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
