mode(1)
//
// Demo of %bigint_a_bigint.sci
//

//
x=bigint('123456789')
y=bigint('987654321')
x+y  //  bigint addition
123456789+987654321 // double addition
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
