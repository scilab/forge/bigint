mode(1)
//
// Demo of %bigint_s_bigint.sci
//

halt()   // Press return to continue
 
y=bigint('123456789')
x=bigint('987654321')
x-y  //  bigint addition
987654321-123456789 // double addition
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
