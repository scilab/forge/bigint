mode(1)
//
// Demo of %s_d_bigint.sci
//

halt()   // Press return to continue
 
x=98765432
y=bigint('12345678')
q=x./y             //  bigint division
98765432/12345678  // verify
(x*ones(2,2))./y   // matrix case
halt()   // Press return to continue
 
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
