mode(1)
//
// Demo of %s_a_bigint.sci
//

//
x=123456789
y=bigint('987654321')
x+y                     // double/bigint addition
x+987654321             // double addition
(x*ones(2,2))+987654321 // matrix double/bigint addition
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
