mode(1)
//
// Demo of %mbigint_s.sci
//

x=bigint('123456789')
y=bigint('0')
z=bigint('1234567890')
M=[x y; z x-z]
-M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
