mode(1)
//
// Demo of %mbigint_p_bigint.sci
//

halt()   // Press return to continue
 
format('v',20)
M=mbigint([1 2 ; 3 4])
e=bigint(10)
M^e
[1 2 ; 3 4]^10
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
