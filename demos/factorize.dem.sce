mode(1)
//
// Demo of factorize.sci
//

halt()   // Press return to continue
 
n=bigint(2^3*5*17^4)
str=factorize(n)
evstr(str)==n           // verify
[str,P,A]=factorize(n)  // retrive factors and exponents
factorize(43691)        // a prime number
n=prod(primes(17))*prod(primes(9))*bigint(1e8)
factorize(n)       // = 2^(10)*3^(2)*5^(10)*7^(2)*11*13*17
// stop search when p^2 >n ( below last prime tested p=223~ sqrt(51061)  )
n=(bigint(2)^53)*bigint(51061)
factorize(n) // =2^(53)*51061
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
