mode(1)
//
// Demo of %mbigint_prod.sci
//

halt()   // Press return to continue
 
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
L=[x y z]
prod(L) //= x*y*z
x*y*z
M=[L; L]
prod(M)
prod(M,'r') // = [x^2 y^2 z^2]
prod(M,'c') // = [x*y*z;x*y*z]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
