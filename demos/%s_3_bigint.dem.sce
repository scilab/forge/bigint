mode(1)
//
// Demo of %s_3_bigint.sci
//

halt()   // Press return to continue
 
x=12345678
y=bigint('123456789')
x<=y  //  True
x=1234567890
x<=y // false
(x*ones(2,2))<=y  // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
