mode(1)
//
// Demo of %bigint_d_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('987654321')
M=[y x; -x -y]
N=[x x;x x]
x./M
x./N
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
