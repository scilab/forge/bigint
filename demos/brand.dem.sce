mode(1)
//
// Demo of brand.sci
//

halt()   // Press return to continue
 
a=bigint('123456789987654321')
b=bigint('100000000000000000')
x=brand(a)        // x<=a
x=brand(a,b)      // b<=x<=a
A=brand(2,3,a)    // A(i,j)<=a
B=brand(2,3,a,b)  // B(i,j)<= b
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
