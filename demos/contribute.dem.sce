mode(1)
//
// Demo of contribute.sci
//

contribute()  // basic usage
contribute('example') // how to use optional parameters
contribute(1,2)
contribute('a',2,'c')
edit(fullfile(TMPDIR,'contribute')) //  see source code
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
