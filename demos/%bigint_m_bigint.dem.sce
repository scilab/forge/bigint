mode(1)
//
// Demo of %bigint_m_bigint.sci
//

//
x=bigint('12345678')
y=bigint('98765432')
x*y  //  bigint product
12345678*98765432 // double product
x=bigint('123456789987654321')
y=bigint('987654321123456789')
z=x*y
string(z)=='121932632103337905662094193112635269'
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
