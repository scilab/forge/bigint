mode(1)
//
// Demo of %mbigint_i_bigint.sci
//

halt()   // Press return to continue
 
x=bigint(2)^57-1
x(2:3,4:6)=mbigint(ones(2,3))
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
