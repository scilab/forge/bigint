mode(1)
//
// Demo of %s_f_bigint.sci
//

//
x=1
y=bigint('123456789')
L=[x;y]
x=[1:10]'   // matrix case
L=[x;y]
x=[]        // empty case
L=[x;y]
//========= E N D === O F === D E M O =========//
