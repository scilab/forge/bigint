mode(1)
//
// Demo of %s_s_bigint.sci
//

//
x=123456789
y=bigint('987654321')
x-y  //  scalar/bigint soustraction
123456789-987654321 // double soustraction
(x*ones(2,2))-y    //  matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
