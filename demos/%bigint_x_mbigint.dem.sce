mode(1)
//
// Demo of %bigint_x_mbigint.sci
//

//
x=123456
y=456789
z=987654
M=[x y; z x-z]
M=mbigint(MM)
x*MM
bigint(x)*M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
