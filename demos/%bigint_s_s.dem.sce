mode(1)
//
// Demo of %bigint_s_s.sci
//

//
x=bigint('123456789')
y=987654321
x-y  //  scalar/bigint soustraction
123456789-987654321 // double soustraction
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
