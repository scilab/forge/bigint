mode(1)
//
// Demo of %bigint_c_s.sci
//

//
x=bigint('123456789')
y=1
L=[x y]
y=[1:10]   // matrix case
L=[x y]
y=[]        // empty case
L=[x y]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
