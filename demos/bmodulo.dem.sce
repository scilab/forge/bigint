mode(1)
//
// Demo of bmodulo.sci
//

//
x=bigint('1234567898765543210')
y=bigint('9876543210123456789')
[q,r]=divide(y,x)
bmodulo(y,x)// = r
bmodulo([x,y],x)//= [0,r]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
