//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
x=bigint('98765432') 
y=bigint('12345678') 
[q,r]=divide(x,y)
assert_checktrue(q==int(98765432/12345678))
assert_checktrue(r==pmodulo(98765432,12345678))
// tests with integers >10^16 
y=bigint('1234567890')
x=bigint('987654321123456789')
[q,r]=divide(x,y)
assert_checkequal(x,q*y+r)
assert_checktrue(r<abs(y))
// tests with %inf
y=%inf
x=bigint('987654321123456789')
[q,r]=divide(x,y)
assert_checkequal(x,r)
assert_checkequal(q,0)//also bigint(0) ?!?
// tests errors
assert_checkerror("divide(x,0)",'division by 0 !')
