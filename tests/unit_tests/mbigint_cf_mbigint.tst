//<-- CLI SHELL MODE -->
//=================================
// 
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
xx=123456789;
yy=987654321;
zz=xx-yy;
O=bigint(0);
L=[x y];
M=[z O];
N=[O z];
A=[L;M];
P=[x;z];
Q=[y;O];
R=[O;y]
B=[P Q];
assert_checkequal(A,B)
assert_checkfalse(and(L==M))
//overloading with double
MM=[z 0];
assert_checkequal(M,MM)
NN=[0 z];
assert_checkequal(N,NN)
assert_checkfalse(and(MM==NN))
QQ=[y;0];
assert_checkequal(Q,QQ)
RR=[0;y];
assert_checkequal(R,RR)
assert_checkfalse(and(RR==QQ))
AA=[L;[zz 0]]
assert_checkequal(AA,B)
BB=[P [yy;0]];
assert_checkequal(A,BB)
LL=[L []];
assert_checkequal(L,LL)
LL=[L;[]];
assert_checkequal(L,LL)
LL=[[] L];
assert_checkequal(L,LL)
LL=[[];L];
assert_checkequal(L,LL)
MM=[M; []];
assert_checkequal(M,MM)
MM=[[]; M];
assert_checkequal(M,MM)
MM=[M []];
assert_checkequal(M,MM)
MM=[[] M];
assert_checkequal(M,MM)


