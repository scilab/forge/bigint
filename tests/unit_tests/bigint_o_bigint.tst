//<-- CLI SHELL MODE -->
//=================================
x=bigint('1234567890');
assert_checktrue(x==x)
y=bigint('-1234567890');
assert_checkfalse(x==y)
//mixed case
assert_checktrue(x==1234567890)
assert_checktrue(1234567890==x)
assert_checkfalse(1234567890==y)
assert_checkfalse(y==1234567890)
//matrix case
assert_checktrue(and(x==(1234567890*ones(2,2))))
assert_checktrue(and((1234567890*ones(2,2))==x))
assert_checkfalse(and((1234567890*ones(2,2))==y))
assert_checkfalse(and(y==(1234567890)*ones(2,2)))

