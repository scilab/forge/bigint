//<-- CLI SHELL MODE -->
//=================================

//check if  sqrt(bigint)=int(sqrt(double))
for k=8:15
    p10k=bigint(10)^(k-1)
    x=brand(10*p10k,p10k)
    assert_checktrue(floor(sqrt(iconvert(x,0)))==sqrt(x))
end
//check if sqrt(x^2)=x
k=10
p10k=bigint(10)^(k-1)
x=brand(10*p10k,p10k)
r=sqrt(x^2)
assert_checktrue(r==x)

//check if sqrt(x)^2<= x<(sqrt(x)+1)^2
for k=[20 23]
    p10k=bigint(10)^(k-1)
    x=brand(10*p10k,p10k)
    r=sqrt(x)
    assert_checktrue(r^2<= x)
    assert_checktrue((r+1)^2> x)
end
