//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 

N=100;
L=mbigint(1:N)
s=sum(L)
assert_checkequal(s,bigint(N*(N+1)/2))
M=zeros(10,10)
M(:)=1:N
S1=mbigint(sum(M))
S2=mbigint(sum(M,'c'))
S3=mbigint(sum(M,'r'))
M=mbigint(M)
s1=sum(M)
assert_checkequal(s1,S1)
s2=sum(M,'c') 
assert_checkequal(s2,S2)
s3=sum(M,'r')
assert_checkequal(s3,S3)
//scalar case
NN=bigint(N)
assert_checkequal(NN,sum(NN))
