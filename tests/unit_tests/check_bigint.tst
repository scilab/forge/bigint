//<-- CLI SHELL MODE -->
//=================================
x=mlist(['bigint' 'signe' 'rep'], 1,[3456789;12]);
x=check_bigint(x) // OK
x=bigint('-1234567890')
x=check_bigint(x) // OK
x=bigint('0')
x.signe=-1
x=check_bigint(x) // change signe to +
assert_checkequal(x.signe,1)
//check errors
x=mlist(['blop' 'signe' 'rep'], 1,[3456789;12]);
assert_checkerror("x=check_bigint(x)","not bigint structure")
// signe
x=bigint('-1234567890')
x.signe=%pi
 assert_checkerror("x=check_bigint(x)","bigint signe field should be +1 or -1")
//
x=bigint('-1234567890')
x.rep=poly(0,'x')
 assert_checkerror("x=check_bigint(x)","bigint rep should be a vector of 7 digits positives integers")
 //
 x=bigint('-1234567890')
x.rep=1234567890
 assert_checkerror("x=check_bigint(x)","bigint rep should be a vector of 7 digits positives integers")

