//<-- CLI SHELL MODE -->
//=================================
// construct mbigint as an mlist
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
T=cell();
T{1,1}=x;
T{1,2}=y;
T{2,1}=z;
T{2,2}=bigint(0);
M=mlist(['mbigint','display','value'],[string(x) string(y); string(z) '0' ],T)
// construct mbigint by concatenation
U=[x y; z bigint(0)]
// check if equal
assert_checkequal(typeof(M),typeof(U))
assert_checktrue(and(U==M))
assert_checktrue(and(U.display==M.display))
assert_checktrue(and(U.value==M.value))
// construct mbigint with "mbigint.sci"
V=mbigint([123456789, 987654321; 123456789-987654321, 0])
// check if equal
assert_checkequal(typeof(M),typeof(V))
assert_checktrue(and(V==M))
assert_checktrue(and(V.display==M.display))
assert_checktrue(and(V.value==M.value))
// construct mbigint with "mbigint.sci"
V=mbigint(2,2,123456789, 123456789-987654321,987654321,  0)
// check if equal
assert_checkequal(typeof(M),typeof(V))
assert_checktrue(and(V==M))
assert_checktrue(and(V.display==M.display))
assert_checktrue(and(V.value==M.value))

//  insertion extraction
assert_checktrue(and(M(:,1)==[x;z]))
assert_checktrue(and(M(2,:)==[z 0]))
assert_checktrue(M(2,1)==z)
M(2,2)=x*x
// check errors
assert_checkerror("mbigint(2,2,1,2)","number of  coefficients M(i,j)< 4")

