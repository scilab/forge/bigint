//<-- CLI SHELL MODE -->
//=================================

 x=bigint('123456789') 
 y=bigint('02345678') 
 z=bigint('1234567890') 
 M=[x y; z x-z]
 N=[x x; x x]
 assert_checkequal((M==N),[%T %F;%F %F])
 N=[y y; y y]
 assert_checkequal((M==N),[%F %T;%F %F])
 N=[z z; z z]
 assert_checkequal((M==N),[%F %F;%T %F])
 N=[x-z x-z; x-z x-z]
 assert_checkequal((M==N),[%F %F;%F %T])
// mixed bigint / mbigint
 assert_checkequal((M==x),[%T %F;%F %F])
 assert_checkequal((M==y),[%F %T;%F %F])
 assert_checkequal((M==z),[%F %F;%T %F])
 assert_checkequal((M==x-z),[%F %F;%F %T])
 
 
 assert_checkequal((x==M),[%T %F;%F %F])
 assert_checkequal((y==M),[%F %T;%F %F])
 assert_checkequal((z==M),[%F %F;%T %F])
 assert_checkequal((x-z==M),[%F %F;%F %T])
// mixed bigint / double expressions
 assert_checkequal((M==123456789),[%T %F;%F %F])
 assert_checkequal((M==02345678),[%F %T;%F %F])
 assert_checkequal((M==1234567890),[%F %F;%T %F])
 assert_checkequal((M==-1111111101),[%F %F;%F %T])
 
 assert_checkequal((123456789==M),[%T %F;%F %F])
 assert_checkequal((02345678==M),[%F %T;%F %F])
 assert_checkequal((1234567890==M),[%F %F;%T %F])
 assert_checkequal((-1111111101==M),[%F %F;%F %T])

