//<-- CLI SHELL MODE -->
//=================================

x=bigint(3)^41
xx=bigint("36472996377170786403")
assert_checkequal(x,xx)
y=123456^bigint(5)
yy=bigint("28678802168634497644363776")
assert_checkequal(y,yy)
z=bigint(13)^bigint(31)
zz=bigint("34059943367449284484947168626829637")
assert_checkequal(z,zz)

