//<-- CLI SHELL MODE -->
//=================================

 x=bigint('123456789') 
 y=bigint('02345678') 
 z=bigint('1234567890') 
 M=[x y; z x-z]
 N=[-y y; -z x]
NN=[-2345678,2345678 ; -1234567890,123456789]//=N
 P=[x+y bigint(0); 2*z -z]//=M-N
 Q=[-2*y  bigint(0); -z-y x-y]//=N-y
 assert_checkequal((M-N),P)
 assert_checkequal((M-NN),P)
 assert_checkequal((NN-M),-P)
 assert_checkequal((N-y),Q)
 assert_checkequal((y-N),-Q)
 assert_checkequal((N-2345678),Q)
 assert_checkequal((2345678-N),-Q)

