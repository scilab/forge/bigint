//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
x=bigint('98765432') 
y=bigint('12345678') 
d=PGCD(x,y) 
a=x/d
b=y/d
z=PGCD(a,b) // =1
assert_checktrue(z==1)
// tests with integers >10^16 
x=bigint('123456789987654321')
y=bigint('987654321123456789')
z=PGCD(x,y);
assert_checkequal(z,bigint(1222222221))
