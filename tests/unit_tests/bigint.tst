//<-- CLI SHELL MODE -->
//=================================
x=mlist(['bigint' 'signe' 'rep'], 1,[3456789;12])
xx=bigint('123456789')
xxx=bigint(123456789)
assert_checkequal(x,xx)
assert_checkequal(x,xxx)
//negative
x=mlist(['bigint' 'signe' 'rep'], -1,[3456789;12])
xx=bigint('-123456789')
xxx=bigint(-123456789)
assert_checkequal(x,xx)
assert_checkequal(x,xxx)
// zero case
x=mlist(['bigint' 'signe' 'rep'], 1,[0])
xx=bigint('0000000000000000')
xxx=bigint('-00')
assert_checkequal(x,xx)
assert_checkequal(x,xxx)
// %inf case
x=%inf
xx=bigint(x)
assert_checkequal(x,xx)
x=-%inf
xx=bigint(x);
assert_checkequal(x,xx)

