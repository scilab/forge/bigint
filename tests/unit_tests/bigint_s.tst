//<-- CLI SHELL MODE -->
//=================================
x=bigint('1234567890');
y=-x;
x.signe=-1;
assert_checkequal(x,y)
x=bigint('-1234567890');
y=-x;
x.signe=1;
assert_checkequal(x,y)
x=bigint('0');
y=-x;
assert_checkequal(x,y)

