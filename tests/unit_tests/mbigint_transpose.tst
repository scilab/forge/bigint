//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
x=bigint(123456789)
y=bigint(987654321)
L1=[x y x]
L2=[x;y;x]
// strange bug with assert_checkequal
//assert_checkequal(L1',L2)
//L=L1';assert_checktrue(and(L==L2))
L=L1';
assert_checkequal(L.display,L2.display)
assert_checkequal(L.value,L2.value)
//assert_checktrue(L1,L2')
L=L2';
assert_checkequal(L.display,L1.display)
assert_checkequal(L.value,L1.value)
//assert_checktrue(L1.',L2)
L=L1.';
assert_checkequal(L.display,L2.display)
assert_checkequal(L.value,L2.value)
//assert_checktrue(L1,L2.')
L=L2.';
assert_checkequal(L.display,L1.display)
assert_checkequal(L.value,L1.value)
////
M=[x y; x-y bigint(0)]
N=[x x-y; y bigint(0)]
//same bug
//assert_checkequal(M',N)
//P=M';assert_checktrue(and(P==N))
P=M';
assert_checkequal(P.display,N.display)
assert_checkequal(P.value,N.value)
// assert_checkequal(M,N')
P=N';
assert_checkequal(P.display,M.display)
assert_checkequal(P.value,M.value)
// assert_checkequal(M.',N)
P=M.';
assert_checkequal(P.display,N.display)
assert_checkequal(P.value,N.value)
//assert_checkequal(M,N.')
P=N.';
assert_checkequal(P.display,M.display)
assert_checkequal(P.value,M.value)
