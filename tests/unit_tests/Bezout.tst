//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
x=bigint('98765432') 
y=bigint('12345678') 
[d,a,b]=Bezout(x,y) 
assert_checktrue(d==a*x+b*y)
// tests with integers >10^16 
x=bigint('123456789987654321')
y=bigint('987654321123456789')
[d,a,b]=Bezout(x,y) 
assert_checktrue(d==a*x+b*y)
// tests negatives integers 
x=bigint('123456789987654321')
y=bigint('-987654321123456789')
[d,a,b]=Bezout(x,y) 
assert_checktrue(d==a*x+b*y)
