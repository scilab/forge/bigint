//<-- CLI SHELL MODE -->
//=================================

 x=bigint('123456789') 
 y=bigint('02345678') 
 z=bigint('1234567890') 
 M=[x y; z x-z]
 N=[x x; x x]
 assert_checkequal((N>M),[%F %T;%F %T])
 N=[y y; y y]
 assert_checkequal((N>M),[%F %F;%F %T])
 N=[z z; z z]
 assert_checkequal((N>M),[%T %T;%F %T])
 N=[x-z x-z; x-z x-z]
 assert_checkequal((x-z>M),[%F %F;%F %F])
// mixed bigint / mbigint
 assert_checkequal((x>M),[%F %T;%F %T])
 assert_checkequal((y>M),[%F %F;%F %T])
 assert_checkequal((z>M),[%T %T;%F %T])
 assert_checkequal((x-z>M),[%F %F;%F %F])


 assert_checkequal((M>x),[%f %f;%t %f])
 assert_checkequal((M>y),[%t %f;%t %f])
 assert_checkequal((M>z),[%f %f;%f %f])
 assert_checkequal((M>x-z),[%t %t;%t %f])

// mixed bigint double expressions
 assert_checkequal((123456789>M),[%F %T;%F %T])
 assert_checkequal((02345678>M),[%F %F;%F %T])
 assert_checkequal((1234567890>M),[%T %T;%F %T])
 assert_checkequal((-1111111101>M),[%F %F;%F %F])


 assert_checkequal(M>(123456789),[%f %f;%t %f])
 assert_checkequal((M>02345678),[%t %f;%t %f])
 assert_checkequal((M>1234567890),[%f %f;%f %f])
 assert_checkequal((M>-1111111101),[%t %t;%t %f])
