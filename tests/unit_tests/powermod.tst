//<-- CLI SHELL MODE -->
//=================================
// tests with bigint
a=bigint(2)
e=bigint(100) 
n=bigint('100000000')
r=powermod(a,e,n)
b=a^e
[q,rr]=divide(b,n)
assert_checktrue(r==rr)
// tests mixed double/bigint
a=2
e=bigint(100) 
n=bigint(100000000)
r=powermod(a,e,n)
[q,rr]=divide(a^e,n)
assert_checktrue(r==rr)
//
a=bigint(2)
e=100
n=bigint(100000000)
r=powermod(a,e,n)
[q,rr]=divide(a^e,n)
assert_checktrue(r==rr)
//
a=bigint(2)
e=bigint(100) 
n=100000000
r=powermod(a,e,n)
[q,rr]=divide(a^e,n)
assert_checktrue(r==rr)
