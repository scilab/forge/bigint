//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
n=bigint(2^3*5*17^4)
 [str,P,A]=factorize(n)
 assert_checktrue(evstr(str)==n)
 assert_checktrue(prod(P.^A)==n)
 n=bigint(43691)
 [str,P,A]=factorize(n)
 assert_checktrue(evstr(str)==n)
 assert_checktrue(prod(P.^A)==n)
 n=prod(primes(17))*prod(primes(9))*bigint(1e8)
 [str,P,A]=factorize(n)
 assert_checktrue(prod(P.^A)==n)
  // stop search when p^2 >n 
 n=(bigint(2)^53)*bigint(51061)
 [str,P,A]=factorize(n)
 assert_checktrue(prod(P.^A)==n)
