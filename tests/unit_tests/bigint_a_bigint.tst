//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
x=bigint('12345678') ;
y=bigint('98765432') ;
z=x+y;
zz=12345678+98765432;
zzz=bigint(zz)
assert_checkequal(z,zzz)
// tests with  integers <0 
x=bigint('-12345678') ;
y=bigint('98765432') ;
z=x+y;
zz=98765432-12345678;
zzz=bigint(zz)
assert_checkequal(z,zzz)
z=y+x
assert_checkequal(z,zzz)
x=bigint('-12345678') ;
y=bigint('-98765432') ;
z=x+y;
zz=-98765432-12345678;
zzz=bigint(zz)
assert_checkequal(z,zzz)
// tests with integers >10^16 
x=bigint('123456789987654321');
y=bigint('987654321123456789');
z=x+y;
assert_checkequal(z,bigint('1111111111111111110'))
// mixed tests double/bigint
x=bigint('123456789987654321');
y=987654321;
z=x+y;
assert_checkequal(z,bigint('123456790975308642'))
z=y+x;
assert_checkequal(z,bigint('123456790975308642'))
y=987654321*ones(2,2);
z=x+y;
assert_checkequal(z,bigint('123456790975308642')*ones(2,2))
z=y+x;
assert_checkequal(z,bigint('123456790975308642')*ones(2,2))

