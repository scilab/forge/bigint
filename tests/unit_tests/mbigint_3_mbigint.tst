//<-- CLI SHELL MODE -->
//=================================

 x=bigint('123456789') 
 y=bigint('02345678') 
 z=bigint('1234567890') 
 M=[x y; z x-z]
 N=[x x; x x]
 assert_checkequal((M<=N),[%T %T;%F %T])
 N=[y y; y y]
 assert_checkequal((M<=N),[%F %T;%F %T])
 N=[z z; z z]
 assert_checkequal((M<=N),[%T %T;%T %T])
 N=[x-z x-z; x-z x-z]
 assert_checkequal((M<=N),[%F %F;%F %T])
// mixed bigint / mbigint
 assert_checkequal((M<=x),[%T %T;%F %T])
 assert_checkequal((M<=y),[%F %T;%F %T])
 assert_checkequal((M<=z),[%T %T;%T %T])
 assert_checkequal((M<=x-z),[%F %F;%F %T])
 
 
 assert_checkequal((x<=M),[%t %f;%t %f])
 assert_checkequal((y<=M),[%t %t;%t %f])
 assert_checkequal((z<=M),[%f %f;%t %f])
 assert_checkequal((x-z<=M),[%t %t;%t %t])
// mixed bigint / double expressions
 assert_checkequal((M<=123456789),[%T %T;%F %T])
 assert_checkequal((M<=02345678),[%F %T;%F %T])
 assert_checkequal((M<=1234567890),[%T %T;%T %T])
 assert_checkequal((M<=-1111111101),[%F %F;%F %T])
 
 assert_checkequal((123456789<=M),[%t %f;%t %f])
 assert_checkequal((02345678<=M),[%t %t;%t %f])
 assert_checkequal((1234567890<=M),[%f %f;%t %f])
 assert_checkequal((-1111111101<=M),[%t %t;%t %t])
