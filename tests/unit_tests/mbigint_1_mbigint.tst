//<-- CLI SHELL MODE -->
//=================================

 x=bigint('123456789') 
 y=bigint('02345678') 
 z=bigint('1234567890') 
 M=[x y; z x-z]
 N=[x x; x x]
 assert_checkequal((M<N),[%F %T;%F %T])
 N=[y y; y y]
 assert_checkequal((M<N),[%F %F;%F %T])
 N=[z z; z z]
 assert_checkequal((M<N),[%T %T;%F %T])
 N=[x-z x-z; x-z x-z]
 assert_checkequal((M<N),[%F %F;%F %F])
// mixed bigint / mbigint
 assert_checkequal((M<x),[%F %T;%F %T])
 assert_checkequal((M<y),[%F %F;%F %T])
 assert_checkequal((M<z),[%T %T;%F %T])
 assert_checkequal((M<x-z),[%F %F;%F %F])

 assert_checkequal((x<M),[%f %f;%t %f])
 assert_checkequal((y<M),[%t %f;%t %f])
 assert_checkequal((z<M),[%f %f;%f %f])
 assert_checkequal((x-z<M),[%t %t;%t %f])
// mixed bigint / double expressions
 assert_checkequal((M<123456789),[%F %T;%F %T])
 assert_checkequal((M<02345678),[%F %F;%F %T])
 assert_checkequal((M<1234567890),[%T %T;%F %T])
 assert_checkequal((M<-1111111101),[%F %F;%F %F])
 
 assert_checkequal((123456789<M),[%f %f;%t %f])
 assert_checkequal((02345678<M),[%t %f;%t %f])
 assert_checkequal((1234567890<M),[%f %f;%f %f])
 assert_checkequal((-1111111101<M),[%t %t;%t %f])
