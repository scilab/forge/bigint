//<-- CLI SHELL MODE -->
//=================================

x=brand(1e12)
assert_checktrue(x<=1e12)
assert_checktrue(length(x)==1)
x=brand(1e12,1e11)
assert_checktrue((x<=1e12)&(1e+11<=x))
assert_checktrue(length(x)==1)
//matrix
x=brand(2,3,1e12)
assert_checktrue(and(x<=1e12))
assert_checktrue(and(size(x)==[2,3]))
x=brand(4,3,1e12,1e11)
assert_checktrue(and((x<=1e12)&(1e+11<=x)))
assert_checktrue(and(size(x)==[4,3]))
// error
assert_checkerror('brand(1,2,3,4,5)','wrong number of arguments!')
