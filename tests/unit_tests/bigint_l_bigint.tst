//<-- CLI SHELL MODE -->
//=================================

x=bigint('987654321')
y=bigint('123456789')
q=y\x  //  bigint division
qq=bigint(int(987654321/123456789))
assert_checkequal(q,qq)
r=x-q*y
rr=bigint(pmodulo(987654321,123456789))
assert_checkequal(r,rr)
assert_checktrue(r<abs(y))
//overloading compatibility
x=987654321
y=bigint('123456789')
q=y\x  //  bigint division
qq=bigint(int(987654321/123456789))
assert_checkequal(q,qq)
r=x-q*y
rr=bigint(pmodulo(987654321,123456789))
assert_checkequal(r,rr)
assert_checktrue(r<abs(y))
//overloading compatibility
x=bigint('987654321')
y=123456789
q=y\x  //  bigint division
qq=bigint(int(987654321/123456789))
assert_checkequal(q,qq)
r=x-q*y
rr=bigint(pmodulo(987654321,123456789))
assert_checkequal(r,rr)
assert_checktrue(r<abs(y))
//interger greather than 10^16
y=bigint('1234567890')
x=bigint('987654321123456789')
q=y\x  //  bigint division
r=x-q*y
assert_checkequal(x,y*q+r)
assert_checktrue(r<abs(y))

