function [bool]=%bigint_1_s(x,y)
    // overloading  compatibility  for &#60; (lower) operator with bigint/double
    // Calling Sequence
    //       x<y
    //   bool=(x<y)
    //
    // Parameters
    // x : bigint
    // y : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint lower operator
    //
    // Examples
    //
    // x=bigint('123456789')
    // y=1234567890
    // x<y  //  True
    // y=12345678
    // x<y // false
    // 
    // See also
    //  bigint
    //  bigint_1_bigint
    //  bigint_1_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert y to bigint
    y=mbigint(y)
    bool=(x<y)
endfunction
