function [z]=%bigint_p_s(x,y)
    // overloading  compatibility  for #94; ( power) operator with bigint/double
    // Calling Sequence
    //   z=x^y
    //
    // Parameters
    //  x,z: bigint
    // y : double(scalar)
    //
    // Description
    // bigint/double exponentiation ( power)
    //
    // Examples
    // format('v',20)
    // x=5
    // y=bigint('20')
    // x^y
    // 5^20
    // 
    // See also
    //  mbigint
    // bigint_p_bigint
    // bigint_p_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert y to bigint
    y=mbigint(y)
    z=x^y
endfunction
