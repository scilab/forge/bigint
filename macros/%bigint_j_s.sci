function [C]=%bigint_j_s(A,B)
    // overloading compatibility for binary operators .&#94; (element wise power) with bigint/double
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  A : bigint
    //  B : double
    //  C : mbigint
    //
    // Description
    // bigint/double  "element wise power" operator
    //
    // Examples
    // //
    // x=bigint('45') 
    // M=[0 1 ; 2 3]
    // M.^x
    // 2^x
    // 3^x
    // 
    // See also
    //  mbigint
    // mbigint_j_mbigint
    // bigint_j_bigint
    //
    // Authors
    //  Philippe Roux
    //

    B=mbigint(B)
    C=A.^B
endfunction
