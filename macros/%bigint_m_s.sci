function [z]=%bigint_m_s(x,y)
    // overloading  compatibility for operator * (product) with bigint/double
    // Calling Sequence
    //   z=x*y
    //
    // Parameters
    //  x,z: bigint
    // y : double(scalar)
    //
    // Description
    // bigint/double product
    //
    // Examples
    // //
    // x=bigint('12345678')
    // y=98765432
    // x*y  //  scalar/bigint product
    // 12345678*98765432 // double product
    // 
    // See also
    //  mbigint
    // bigint_m_bigint
    // bigint_m_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    y=mbigint(y)
    z=x*y
endfunction
