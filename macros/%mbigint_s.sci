function [B]=%mbigint_s(A)
    // unary operator - (minus) for mbigint
    // Calling Sequence
    //   B=-A
    //
    // Parameters
    //  A,B: bigint
    //
    // Description
    //  mbigint opposite
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('0') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // -M
    // 
    // See also
    //  mbigint
    // bigint_s
    //
    // Authors
    //  Philippe Roux
    //
    
    [p,n]=size(A)
        B=A
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=-a
                B(i,j)=b
            end
        end
endfunction
