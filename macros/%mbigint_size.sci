function [varargout]=%mbigint_size(M,varargin)
    // overloading  compatibility for size for mbigint
    // Calling Sequence
    //   [p[,n]]=size(M[,opt])
    //
    // Parameters
    //  M: mbigint
    //  opt : double or integer
    //  p,n : integers
    //
    // Description
    // matrix size :
    //<itemizedlist>
    // <listitem><para> p= number of rows if opt='r' or 1(default)</para></listitem>
    // <listitem><para> n=number of columns if opt='c' or 2</para></listitem>
    // </itemizedlist>
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // y=bigint('-98765432') 
    // M=[x y; x-y bigint(0)]
    // N=[M,M]
    // [p,n]=size(M) // = p=2,n=2
    // [p,n]=size(N) // = p=2,n=4
    // p=size(N,1) // = p=2,n=2
    // n=size(N,2) // = p=2,n=2
    // 
    // See also
    //  size
    //
    // Authors
    //  Philippe Roux
    //

    [lhs,rhs]=argn()
    if (rhs>1) then opt=varargin(1)
        else opt=0
    end
    [p,n]=size(M.display)
    if opt==0 then
        if lhs==2 then
            varargout=list(p,n)
        elseif lhs==1 then 
            varargout=list([p,n])
            else error('not implemented')
        end
        elseif (opt==1)|(opt=='r') then 
        varargout=list(p)
        elseif (opt==2)|(opt=='c') then 
        varargout=list(n)
        else warning('option not implemented!')
        varargout=list(p,n)
    end
    if (rhs>1)&(lhs>1) then 
        error('only one output !')
    end
endfunction
