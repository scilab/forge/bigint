function [y]=%bigint_log10(x)
    // bigint decimal logarithm
    // Calling Sequence
    //   [y]=log10(x)
    //
    // Parameters
    //  x,y: bigint
    //
    // Description
    // y is the integer part plus one of the decimal logarithm of x , i.e. the number of digits in decimal representation :
    //
    //  <latex>\forall  x\in {\mathbb N},\,\, 10^{y-1} \leq x\lneq 10^{y}</latex>
    //
    // the algorithm use the Newton method , i.e. it computes the sequence
    //
    //  <latex>x_{n+1}=x_n-1+{x\over 10^{x_n}}</latex>
    // until <latex>x_{n+1}\geq x_n</latex>
    //
    // Examples
    // 
    // log10(9876543210)
    // log10(bigint(9876543210))
    // 
    // See also
    //  bigint
    // log10
    // abs
    //
    // Authors
    //  Philippe Roux
    //

    // check x>0
    if x<0 then error(' logarithm of negative bigint not implemented!')
    elseif type(x)==1 then
        y=int(log10(x))+1
    elseif x<1e15 then
        y=int(log10(iconvert(x,0)))+1// iconvert -> double ?bug in scilab6?
    else     //  Newton algorithm
        x0=x
        y=x0
        x=bigint(evstr('int(log10('+string(x0)+'))'))
        while x<y
            y=x
            [q,r]=divide(x0,10^x)
            x=x-1+q
        end
        y=y+1 // above rounding
    end
endfunction
