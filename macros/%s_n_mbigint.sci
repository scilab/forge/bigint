function [bool]=%s_n_mbigint(x,A)
     // overloading  compatibility for binary operator " &#60;&#62;" (not equal)with double/mbigint
    // Calling Sequence
    //   x<>A
    //
    // Parameters
    //  A: mbigint
    //  x: double
    //  bool : boolean
    //
    // Description
    // double/mbigint "not equal" comparison
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // A=[x y]
    // B=[y x]
    // C=[x x]
    // 123456789<>A  
    // 123456789<>B  
    // 123456789<>C
    // [123456789, 987654321]<>A // matrix case
    // 
    // See also
    //  mbigint
    // bigint_o_mbigint
    // mbigint_o_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    x=mbigint(x)
    bool=~(x==A)
endfunction
