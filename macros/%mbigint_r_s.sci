function [B]=%mbigint_r_s(A,x)
    // overloading compatibility for binary operators / (right divide) with bigint/mbigint
    // Calling Sequence
    //   B=A/x
    //
    // Parameters
    //  A,B: mbigint
    //    x: double
    //
    // Description
    // mbigint "right divide" operator. The case of x= matrix is not yet implemented ...
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // z=123456789
    // M=[y x; -x -y]
    // N=[x x;x x]
    // M/z
    // N/z
    // 
    // See also
    //  mbigint
    // mbigint_r_bigint
    // mbigint_r_mbigint
    //
    // Authors
    //  Philippe Roux
    //


    x=mbigint(x) //  
     B=A/x
    endfunction
