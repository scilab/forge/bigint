function [B]=%mbigint_a_s(A,x)
    // overloading compatibility for binary operator + (plus) with mbigint/double
    // Calling Sequence
    //   B=A+x
    //
    // Parameters
    //  A,B: mbigint
    //  x : double
    //
    // Description
    // mbigint/double "plus" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M+123456789             // scalar case
    // M+(123456789*ones(2,2)) // matrix case
    // 
    // See also
    //  mbigint
    // mbigint_a_bigint
    // mbigint_a_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    B=A+x
    endfunction
