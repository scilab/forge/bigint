function [bool]=%mbigint_1_bigint(A,x)
    // overloading compatibility for binary operator &#60; (lower) with mbigint/bigint
    // Calling Sequence
    //   bool=(A<x)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // mbigint/bigint "lower" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M<x
    // M<y
    // M<z
    // 
    // See also
    //  mbigint
    // bigint_1_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    bool=(zeros(p,n)==0)
    for i=1:p
        for j=1:n
            a=(A.value{i,j}<x)
            bool(i,j)=a
        end
    end
endfunction
