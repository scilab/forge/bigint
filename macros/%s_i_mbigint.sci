function M=%s_i_mbigint(varargin)
    // insertion operator for mbigint matrix
    // Calling Sequence
    //   M(i,j)=x
    //
    // Parameters
    // M : mbigint
    // i,j : integers
    // x : double
    //
    // Description
    // insert element x in the i-th line j-th column of bigint matrix M, you can use  vector notations for indexes as  M(a:b,c) M(a,b:c), M(a:b,c:d), M(a,:) M(:,b), M([...]), M(...)=[]  suppress cells (if compatible with size ....)
    //
    // Examples
    // 
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // M=[x y;x-y bigint(0)]
    // M(2,2)=1    //  change cell (2,2)
    // M(1,1:2)=0
    // M(1:2,2)=-1
    // M(:,1)=2
    // M(1:2,2)=[]  // suppress 2nd column
    // 
    // See also
    //  mbigint
    // bigint_i_mbigint
    // mbigint_i_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    [lhs ,rhs ]=argn()
    M=varargin($)
    x=varargin($-1)
    if x==[] then  //suppress cells
        // indexes  to be kept
        [p,n]=size(M) // old matrix size
        C=zeros(p,n)
        C(:)=1:p*n
        C(varargin(1:$-2))=[] // remove cells
        [pp,nn]=size(C)  // newmatrix size
        ind=C(:)  // index in original matrix
        D=string(zeros(pp,nn)) // new display
        T=cell(pp,nn)         //  new value
        for i=1:pp
            for j=1:nn
                //  k= new index
                D(i,j)=M.display(C(i,j))
                T{i,j}=M.value{C(i,j)}
            end
        end
        M.display=D
        M.value=T
    else // call to bigint_i_mbigint  or mbigint_i_mbigint
        x=mbigint(x)
        M(varargin(1:$-2))=x
    end

endfunction
