function M=%mbigint_c_bigint(L,x)
    //  line concatenation for  mbigint/bigint 
    // Calling Sequence
    //   M=[L x]
    //
    // Parameters
    //  M,L: mbigint
    // x : bigint
    //
    // Description
    // concatenate a mbigint with a bigint, on its right, to a line mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // L=[x y]
    // M=[L y-x]
    // 
    // See also
    //  mbigint
    // mbigint_c_mbigint
    // bigint_c_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    [p,n]=size(L)
    T=cell(p,n+1)
    T{1,n+1}=x
    T(1,1:n)=L.value
    M=mlist(['mbigint','display','value'],..
      [L.display string(x) ],T)  
endfunction
