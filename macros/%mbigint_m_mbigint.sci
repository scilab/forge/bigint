function [C]=%mbigint_m_mbigint(A,B)
    // binary operators * (product) for mbigint
    // Calling Sequence
    //   C=A*B
    //
    // Parameters
    //  A,B,C: mbigint
    //
    // Description
    // mbigint "product" operator
    //
    // Examples
    // //
    // x=123456
    // y=456789
    // z=987654
    // MM=[x y; z x-z]
    // M=mbigint(MM)
    // NN=[x -z; -y x-z]
    // N=mbigint(NN)
    // M*N
    // MM*NN //double verification
    // 
    // See also
    //  mbigint
    // bigint_m_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,k]=size(A)
    [k1,n]=size(B)
    if (k<>k1) then
        error('incompatible size!')
    else
        C=mbigint(zeros(p,n))
        for i=1:p
            for j=1:n
                c=bigint(0)
                for r=1:k
                a=A.value{i,r} 
                b=B.value{r,j}
                c=c+(a*b)
                //typeof(A.value(i,j))= ce(ll) 
                //typeof{A.value(i,j})= bigint
            end
            C(i,j)=c
            end
        end
    end
    endfunction
