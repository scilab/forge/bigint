function [B]=%s_s_mbigint(x,A)
    // overloading compatibility for binary operator - (minus) with double/mbigint
    // Calling Sequence
    //   B=x-A
    //
    // Parameters
    //  A,B: mbigint
    //  x : bigint
    //
    // Description
    // double/mbigint soustraction ("minus" operator)
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // 123456789-M
    // (123456789*ones(2,2))-M // matrix case
    // 
    // See also
    //  mbigint
    // mbigint_a_bigint
    // bigint_a_bigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    B=x+(-A)
    endfunction
