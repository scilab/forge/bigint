function M=%bigint_i_mbigint(varargin)
    // insertion operator () for bigint/mbigint 
    // Calling Sequence
    //   M(i,j)=x
    //
    // Parameters
    // M : mbigint
    // i,j : integers
    // x : bigint
    //
    // Description
    // insert element x in the i-th line j-th column of bigint matrix M. you can use  vector notations for indexes as  M(a:b,c) M(a,b:c), M(a:b,c:d), M(a,:) M(:,b) M([...]) ....
    //
    // Examples
    // 
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // M=[x y;x-y bigint(0)]
    // M(2,2)=y-x
    // M(1,1:2)=-x
    // M(1:2,2)=-y
    // M(1:3)= x   // index instead of line/column
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    M=varargin($)  //mbigint
    x=varargin($-1)// bigint
    xstr=string(x)
    // indexes of cell to be inserted
    [p,n]=size(M)
    B=zeros(p,n)
    B(varargin(1:$-2))=1// a work around to manage index vs line/column
    [I,J]=find(B==1)
    [p1,n1]=size(B)
    // inserting new values
    T=M.value
    D=M.display
    for k=1:length(I)
        i=I(k);j=J(k)
        D(i,j)=xstr
        T{i,j}=x
    end
    M.value=T
    M.display=D
endfunction
