function q=%bigint_q_s(x,y)
    // overloading compatibility binary operator .\ (element wise left divide) for bigint\double
    // Calling Sequence
    //   q=x.\y
    //
    // Parameters
    //  x,q: bigint
    //    y: double
    //
    // Description
    // bigint/double "element wise left divide" reduce to  macro divide .
    //
    // Examples
    // 
    // y=98765432
    // x=bigint('12345678') 
    // q=x.\y  //  bigint division
    // 98765432/12345678
    // r=y-q*x 
    // pmodulo(98765432,12345678) //  division with doubles
    // y== q*x+r
    // r<abs(x)
    // 
    // See also
    //  mbigint
    // bigint_q_bigint
    // bigint_q_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    y=mbigint(y)
    q=x.\y
endfunction

