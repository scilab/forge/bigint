function r=odd(x)
    // bigint odd test
    // Calling Sequence
    //   r=odd(x)
    //
    // Parameters
    //  x: bigint
    //  r: 0 or 1
    //
    // Description
    // returns 0 if x is odd , otherwize 1 
    //
    // Examples
    // 
    // x=bigint('12345678') 
    // y=bigint('123456789') 
    // odd(x) //=0
    // odd(y) //=1
    // 
    // See also
    //  div2
    //
    // Authors
    //  Philippe Roux
    // 
    
    // check if last digit is multiple of 2
    r=pmodulo(x.rep(1),2)
endfunction
