function x=div2(x)
    // bigint divide by 2
    // Calling Sequence
    //   x=div2(x)
    //
    // Parameters
    //  x : bigint
    //
    // Description
    //  compute int(x/2) for bigint
    //
    // Examples
    // 
    // format('v',20)
    // 123456789/2
    // x=bigint(123456789)
    // div2(x)
    // 
    // See also
    //  odd
    //
    // Authors
    //  Philippe Roux
    //
    
    n=length(x.rep)
    reste=0
    for k=n:-1:1
        x.rep(k)=x.rep(k)+reste*1e+7
        reste=pmodulo(x.rep(k),2)
        x.rep(k)=(x.rep(k)-reste)/2
    end
    x=check_bigint(x)
endfunction
