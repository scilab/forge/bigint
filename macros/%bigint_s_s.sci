function [z]=%bigint_s_s(x,y)
    // overloading  compatibility  for - (minus) operator  with bigint/double
    // Calling Sequence
    //   z=x-y
    //
    // Parameters
    //  x,z: bigint
    // y : double(scalar)
    //
    // Description
    // bigint/double soustraction
    //
    // Examples
    // //
    // x=bigint('123456789')
    // y=987654321
    // x-y  //  scalar/bigint soustraction
    // 123456789-987654321 // double soustraction
    // 
    // See also
    //  mbigint
    // bigint_s_bigint
    // bigint_s_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    y=mbigint(y)
    z=x-y
endfunction
