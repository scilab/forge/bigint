function [z]=%bigint_x_bigint(x,y)
    // overloading compatibility for binary operator .* (element wise product) with bigint
    // Calling Sequence
    //   z=x.*y
    //
    // Parameters
    //  x,y,z: bigint
    //
    // Description
    // bigint element wise product reduce to usual product
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // y=bigint('98765432') 
    // x*y  //  bigint product
    // 12345678*98765432 // double product
    // x.*y //  no type error 
    // 
    // See also
    //  bigint
    // bigint_m_bigint
    //
    // Authors
    //  Philippe Roux
    //

    z=x*y
endfunction
