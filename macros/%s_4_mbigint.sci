function [bool]=%s_4_mbigint(x,A)
    // overloading compatibility for &#8805; (greater or equal) operator with double/mbigint
    // Calling Sequence
    //   x>=A
    //   bool=(x>=A)
    //
    // Parameters
    // A : mbigint
    // x : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint "greater or equal" operator
    //
    // Examples
    //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // 123456789>=M
    // 2345678>=M
    // -123456789>=M
    // //matrix case
    // [123456789 2345678; -123456789 0]>=M
    // 
    // See also
    // mbigint
    // bigint_3_mbigint
    // mbigint_3_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    bool=(A<=x)
endfunction

