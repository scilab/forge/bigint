function [z]=%mbigint_p_s(x,y)
    // overloading  compatibility  for #94; ( power) operator with mbigint/double
    // Calling Sequence
    //   z=x^y
    //
    // Parameters
    //  x, z: mbigint
    // y : double(scalar)
    //
    // Description
    // mbigint/double exponentiation ( power)
    //
    // Examples
    // format('v',20)
    // M=mbigint([1 2 ; 3 4])
    // e=10
    // M^e
    // [1 2 ; 3 4]^10
    // 
    // See also
    //  bigint
    // mbigint_p_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert y to bigint
    [p,n]=size(y)
    if p*n>1 then  error('not implemented!')
    end
    y=bigint(y)
    z=x^y
endfunction
