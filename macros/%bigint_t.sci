function x=%bigint_t(x)
    // overloading compatibility for ' "conjugate" operator  with  bigint 
    // Calling Sequence
    //   x'
    //
    // Parameters
    //  x : bigint
    //
    // Description
    // conjugate x'=x  for bigint
    //
    // Examples
    // 
    // x=bigint('123456789') 
    // x'
    // 
    // See also
    //  matrix_bigint
    // mbigint_t
    //
    // Authors
    //  Philippe Roux
    //
    
   // nothing to do !!
endfunction
