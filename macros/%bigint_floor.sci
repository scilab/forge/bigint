function [x]=%bigint_floor(x)
    // overloading compatibility "integer part" (floor) for bigint
    // Calling Sequence
    //   x=floor(x)
    //
    // Parameters
    //  x: bigint
    //
    // Description
    // integer part of bigint
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // floor(x)
    // floor(-x)
    // 
    // See also
    //  bigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
