function [q,r]=divide(x,y)
    // bigint Euclidian arithmetics
    // Calling Sequence
    //   [q,r]=divide(x,y)
    //
    // Parameters
    //  x,y,q,r: bigint
    //
    // Description
    // bigint Euclidian division using dichotomy 
    // <latex> x= q*y+r</latex>  with  <latex> 0\leq r\le|y|</latex> 
    //
    // Examples
    // 
    // x=bigint('98765432') 
    // y=bigint('12345678') 
    // [q,r]=divide(x,y)  //  bigint division
    // 98765432/12345678  // totient 
    // pmodulo(98765432,12345678) // reminder
    // y=bigint('1234567890')
    // x=bigint('987654321123456789')
    // [q,r]=divide(x,y)
    // x== q*y+r
    // r<abs(y)
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //

    // convert to bigint if necessary
    flag_inf=%F
    if type(x)==1 then x=bigint(x)
    end
    //flag_inf=%F  // special case y=%inf
    if type(y)==1 then 
        if isinf(y) then //special case
            y=x+1
            flag_inf=%T
        else y=bigint(y)
        end
    end
    // check division by 0
    if y==0 then error('division by 0 !')
    end
    // reduce to x,y>0 
    if (x.signe==-1)&(y.signe==-1) then 
        [q,r]=divide(-x,-y)  //-x=(-y)*q+r
        if r<>0 then 
            q=q+1;r=-y-r       // x=  y *q-r=y*(q+1)-(y+r)
        //else q=-q  // // -x=  -y *q<=>x=q*y
        end
    elseif (x.signe==1)&(y.signe==-1) then 
        [q,r]=divide(x,-y)   //x=(-y)*  q +r
        q=-q                 //x=  y *(-q)+r
    elseif (x.signe==-1)&(y.signe==1) then 
        [q,r]=divide(-x,y)   //-x=y * q +r
        if r<>0 then  
            q=-(q+1);r=y-r       // x=y*(-q)-r=y*(-q-1)+(y-r)
        else q=-q
        end
    elseif x==0  then //0=0*y+0
        q=bigint(0), r=bigint(0)
    elseif flag_inf then r=x,q=0
    else //x,y>0  use dichotomy to find q=x/y
        q=evstr(string(x))/evstr(string(y))
        n=ceil(log10(q))
        a=bigint(msprintf("%"+string(n)+".0f",floor(q*(1-%eps))))
        b=bigint(msprintf("%"+string(n)+".0f",ceil(q*(1+%eps))))
        while abs(a-b)>1
            m=div2(a+b)
            z=m*y
            if z>x then b=m
            elseif z<x then a=m
            else q=m, break
            end
        end
        q=div2(a+b)
        q=check_bigint(q)
        r=x-q*y  // reminder 
        //  reminder must be positive
        if r<0 then q=q-1, r=r+y
        end
    end
endfunction

