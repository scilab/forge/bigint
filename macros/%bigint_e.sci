function r=%bigint_e(varargin)
    // overloading compatibility extraction operator  with  bigint
    // Calling Sequence
    //   M(i,j)
    //
    // Parameters
    //  M : mbigint
    //  i,j : integers
    //
    // Description
    // extraction element in the i-th line j-th column of bigint matrix M
    //
    // Examples
    // 
    // x=bigint(2)^57-1 
    // x(1)
    // x(1,1)
    // x(2,3)   // invalid index!
    // 
    // See also
    //  bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
      M=varargin($)
      B=0
      B(varargin(1:$-1))=1// a work around to manage index vs line/column
      [p,n]=size(B)
      if p*n>1 then error('Invalid index')
          else r=M
      end
endfunction
