function str=mbigint2latex(M)
    // bigint basics
    // Calling Sequence
    // [str]=mbigint2latex(M)
    //
    // Parameters
    // M: mbigint
    // str: string
    //
    // Description
    // convert mbigint to latex for compatibility with prettyprint 
    //
    // Examples
    //
    // M=brand(2,3,1e+9)
    // string(M)
    //
    // See also
    // string
    // bigint2latex
    // prettyprint
    //
    // Authors
    // Philippe Roux
    //

    str=prettyprint(M.display)
    
endfunction
