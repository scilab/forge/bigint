function [M]=%mbigint_ceil(M)
    // overloading compatibility "integer part" (ceil) for mbigint
    // Calling Sequence
    //   M=ceil(M)
    //
    // Parameters
    //  M: mbigint
    //
    // Description
    // integer part of mbigint
    //
    // Examples
    // //
    // M=brand(2,3,1e5) 
    // ceil(M)
    // ceil(-M)
    // 
    // See also
    //  mbigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
