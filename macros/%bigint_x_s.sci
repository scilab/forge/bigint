function [z]=%bigint_x_s(x,y)
    // overloading  compatibility  for .* (element wise product) with double
    // Calling Sequence
    //   z=x.*y
    //
    // Parameters
    //  x,z: bigint
    // y : double(scalar)
    //
    // Description
    // bigint/double element wise product
    //
    // Examples
    // //
    // x=bigint('12345678')
    // y=98765432
    // x*y  //  scalar/bigint product
    // 12345678*98765432 // double product
    // x.*y  //  no type error
    // 
    // See also
    //  mbigint
    // bigint_x_bigint
    // bigint_x_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    y=mbigint(y)
    z=x.*y
endfunction
