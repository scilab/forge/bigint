function [C]=%mbigint_x_mbigint(A,B)
    // binary operators .* (element wise product) for mbigint
    // Calling Sequence
    //   C=A.*B
    //
    // Parameters
    //  A,B,C: mbigint
    //
    // Description
    // mbigint "element wise product" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('9876543') 
    // x*y
    // M=[x y; -x -x]
    // N=[y x; y -y]
    // M.*N
    // 
    // See also
    //  mbigint
    // bigint_x_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    [p1,n1]=size(B)
    if (p<>p1)|(n<>n1) then
        error('incompatible size!')
    else
        C=A
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=B.value{i,j}
                c=(a*b)
                C(i,j)=c
            end
        end
    end
    endfunction
