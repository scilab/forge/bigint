function [M]=%mbigint_int(M)
    // overloading compatibility "integer part" (int) for mbigint
    // Calling Sequence
    //   M=int(M)
    //
    // Parameters
    //  M: mbigint
    //
    // Description
    // integer part of mbigint
    //
    // Examples
    // //
    // M=brand(2,3,1e5) 
    // int(M)
    // int(-M)
    // 
    // See also
    //  mbigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
