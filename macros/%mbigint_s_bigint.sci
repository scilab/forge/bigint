function [B]=%mbigint_s_bigint(A,x)
    // overloading compatibility for binary operator - (minus) with mbigint/bigint
    // Calling Sequence
    //   B=A-x
    //
    // Parameters
    //  A,B: mbigint
    //  x : bigint
    //
    // Description
    // mbigint/bigint "minus" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M-x
    // 
    // See also
    //  mbigint
    // bigint_s_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    if p*n==1 then
        B=(A.value{1,1})-x
    else
        B=A
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=(a-x)
                B(i,j)=b
            end
        end
    end
    endfunction
