function [B]=%bigint_x_mbigint(x,A)
    // overloading  compatibility for binary operators .* (element wise product) with bigint/mbigint
    // Calling Sequence
    //   B=x*A
    //
    // Parameters
    // A,B: mbigint
    // x : bigint
    //
    // Description
    // bigint/mbigint element wise product
    //
    // Examples
    // // 
    // x=123456
    // y=456789
    // z=987654
    // M=[x y; z x-z]
    // M=mbigint(MM)
    // x*MM
    // bigint(x)*M
    // 
    // See also
    //  bigint
    // bigint_m_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    [p,n]=size(A)
    if p*n==1 then
        B=(A.value{1,1})*x
    else
        D=string(zeros(p,n))
        T=cell(p,n)
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=(a*x)
                T{i,j}=b
                D(i,j)=string(b)
            end
        end
        B=mlist(['mbigint','display','value'],D,T)
    end
endfunction
