function [bool]=%mbigint_1_mbigint(A,B)
    // binary operators &#60; (lower) for mbigint
    // Calling Sequence
    //   bool=(A<B)
    //
    // Parameters
    //  A,B: mbigint
    //  bool : boolean
    //
    // Description
    // mbigint "lower" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x x; x x]
    // N=[x y; z x-z]
    // M<N
    // N<M
    // M<M
    // 
    // See also
    //  mbigint
    // bigint_1_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    [p1,n1]=size(B)
    if (p<>p1)|(n<>n1) then
        error('incompatible size!')
    else
        bool=(zeros(p,n)==0)
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=B.value{i,j}
                bool(i,j)=(a<b)
                //typeof(A.value(i,j))= ce(ll) 
                //typeof{A.value(i,j})= bigint
            end
        end
    end
    endfunction
