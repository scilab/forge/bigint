function M=%bigint_c_mbigint(x,L)
    // left line concatenation [,] for  bigint/mbigint
    // Calling Sequence
    //   M=[x L]
    //
    // Parameters
    // M,L: mbigint
    // x : bigint
    //
    // Description
    // concatenate a bigint with  a line mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // L=[x y]
    // M=[y-x L]
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    [p,n]=size(L)
    T=cell(p,n+1)
    T{1,1}=x
    T(1,2:n+1)=L.value
    M=mlist(['mbigint','display','value'],..
      [string(x) L.display],T)  
endfunction
