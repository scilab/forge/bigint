function [bool]=%bigint_n_mbigint(x,A)
    // overloading compatibility for " &#60;&#62;" (not equal) operator with bigint/mbigint
    // Calling Sequence
    //   x<>A
    //
    // Parameters
    //  A: mbigint
    //  x: bigint
    //  bool : boolean
    //
    // Description
    // bigint/mbigint "not equal" comparison
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // A=[x y]
    // B=[y x]
    // C=[x x]
    // x<>A  
    // x<>B  
    // x<>C
    // 
    // See also
    //  mbigint
    // bigint_o_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    bool=~(x==A)
endfunction
