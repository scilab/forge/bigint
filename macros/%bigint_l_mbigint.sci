function [B]=%bigint_l_mbigint(x,A)
    // overloading compatibility for operator \ (left divide) with bigint/mbigint
    // Calling Sequence
    //   B=x\A
    //
    // Parameters
    //  A,B: mbigint
    //    x: bigint
    //
    // Description
    // bigint/mbigint "left divide" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // M=[y x; -x -y]
    // N=[x x;x x]
    // x\M
    // x\N
    // 
    // See also
    //  mbigint
    // bigint_r_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    if p*n==1 then
        B=(A.value{1,1})/x
    else
        B=A
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                B(i,j)=(a/x)
            end
        end
    end
    endfunction
