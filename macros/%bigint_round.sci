function [x]=%bigint_round(x)
    // overloading compatibility "integer part" (round) for bigint
    // Calling Sequence
    //   x=round(x)
    //
    // Parameters
    //  x: bigint
    //
    // Description
    // integer part of bigint
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // round(x)
    // round(-x)
    // 
    // See also
    //  bigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
