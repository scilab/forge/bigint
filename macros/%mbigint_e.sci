function r=%mbigint_e(varargin)
    // extraction operator  for  mbigint
    // Calling Sequence
    //   M(i,j)
    //
    // Parameters
    //  M : mbigint
    //  i,j : integers
    //
    // Description
    // extraction element in the i-th line j-th column of bigint matrix M
    //
    // Examples
    // 
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // M=[x y;x-y bigint(0)]
    // M(2,2)
    // M(1,:) // first line
    // M(:,2) // second column
    // 
    // See also
    //  bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //

    M=varargin($)
    [p,n]=size(M)
    // verify the size of result
    A=zeros(p,n); 
    A(varargin(1:$-1))=1; 
    K=length(find(A==1))
    if K>1 then // mbigint
        r=mlist(['mbigint','display','value'],M.display(varargin(1:$-1)),M.value(varargin(1:$-1)))
    else // bigint
        r=M.value{varargin(1:$-1)}
        //r=r.entries  // WTF !?! better in scilab-6.0.0
    end
endfunction
