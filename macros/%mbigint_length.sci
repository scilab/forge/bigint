function [l]=%mbigint_length(M)
    //  mbigint   length
    // Calling Sequence
    //   l=length(M)
    //
    // Parameters
    //  M : mbigint
    // l: integer
    //
    // Description
    // length of a  bigint vector (prod(size(M)) for a matrix) 
    //
    // Examples
    // 
    //x=bigint(123456789);
    //y=bigint(987654321);
    //z=x-y;
    // M=[x y z]
    // [p,n]=size(M) // = p=1,n=3
    // length(M) // = 3
    // M=[M; M]
    // [p,n]=size(M) // = p=2,n=3
    // length(M)  //= 6
    // 
    // See also
    //  size
    // mbigint
    // bigint_length
    // mbigint_size
    //
    // Authors
    //  Philippe Roux
    //

    l=prod(size(M.display))
endfunction
