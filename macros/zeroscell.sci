function T=zeroscell(p,n)
    // create cell of bigint(0)
    // Calling Sequence
    //  T=zeroscell(p,n)
    //
    // Parameters
    // T : cell
    // p,n : integers
    //
    // Description
    // create pxn cell with bigint(0) in each cell
    //
    // Examples
    // 
    // zeroscell(2,3)
    // 
    // See also
    //  matrix_bigint
    //  mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    for i=1:p
        for j=1:n
            T{i,j}=bigint(0)
        end
    end
endfunction
