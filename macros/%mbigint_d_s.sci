function [B]=%mbigint_d_s(A,x)
    // overloading compatibility for binary operators ./ (element wise right divide) with mbigint/double
    // Calling Sequence
    //   B=A./x
    //
    // Parameters
    //  A,B: mbigint
    //    x: double
    //
    // Description
    // mbigint/double "element wise right divide" operator. 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // z=123456789
    // M=[y x; -x -y]
    // N=[x x;x x]
    // M./z
    // N./z
    // 
    // See also
    //  mbigint
    // mbigint_d_bigint
    // mbigint_d_mbigint
    //
    // Authors
    //  Philippe Roux
    //


    x=mbigint(x) 
     B=A./x
    endfunction
