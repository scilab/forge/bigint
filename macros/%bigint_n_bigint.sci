function [bool]=%bigint_n_bigint(x,y)
    // binary operator &#60;&#62; (not equal) for bigint
    // Calling Sequence
    //   bool=(x<>y)
    //
    // Parameters
    //  x,y: bigint
    //  bool : boolean
    //
    // Description
    //   difference (not equal) operator for bigint
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('0123456789') 
    // z=bigint('1234567890') 
    // x<>y  // false 
    // x<>z  // true
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    x=check_bigint(x)
    y=check_bigint(y)
    bool=(x.signe<>y.signe)|(or(x.rep<>y.rep))
endfunction
