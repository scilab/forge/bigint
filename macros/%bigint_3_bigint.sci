function [bool]=%bigint_3_bigint(x,y)
    // binary operator &#8804; (lower or equal) for bigint
    // Calling Sequence
    //   bool=(x<=y)
    //
    // Parameters
    //  x,y: bigint
    //  bool : boolean
    //
    // Description
    // bigint  lower or equal operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // x<=y  // false 
    // x<=z  // true
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //

    x=check_bigint(x)
    y=check_bigint(y)
    if (x.signe*y.signe)<0 then
        bool=(x.signe<y.signe)
    elseif x.signe==-1 then
        bool=((-y)<=(-x))
    else //x,y>0
        nx=length(x.rep)
        ny=length(y.rep)
        if nx<>ny then 
            bool=(nx<ny)
        else k=nx
            while (x.rep(k)==y.rep(k))&(k>1)
                k=k-1;
            end
            bool=(x.rep(k)<=y.rep(k))
        end
    end
    endfunction
