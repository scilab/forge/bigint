function [bool]=%mbigint_2_bigint(A,x)
    // overloading compatibility for binary operator &#62; (greater) with mbigint/bigint
    // Calling Sequence
    //   bool=(x>y)
    //
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // mbigint/biginnt "greater" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M>x
    // M>y
    // M>z
    // 
    // See also
    //  mbigint
    // bigint_2_bigint
    //

    [p,n]=size(A)
    bool=(zeros(p,n)==0)
    for i=1:p
        for j=1:n
            bool(i,j)=(A.value{i,j}>x)
        end
    end
    endfunction
