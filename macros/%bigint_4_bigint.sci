function [bool]=%bigint_4_bigint(x,y)
    // binary operator &#8805; (greater or equal) for bigint
    // Calling Sequence
    //   bool=(x>=y)
    //
    // Parameters
    //  x,y: bigint
    //  bool : boolean
    //
    // Description
    // bigint "greater or equal" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('0123456789') 
    // z=bigint('1234567890') 
    // x>=y  // true 
    // x>=z  // false
    // 
    // See also
    //  bigint
    // bigint_3_bigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(y<=x)  // see %bigint_3_bigint
    endfunction
