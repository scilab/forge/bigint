function [y]=%bigint_sqrt(x)
    // bigint square root
    // Calling Sequence
    //   [y]=sqrt(x)
    //
    // Parameters
    //  x,y: bigint
    //
    // Description
    // y is the integer part of the square root of x
    //
    //  <latex>\forall  x\in {\mathbb N},\,\, y^2 \leq x\lneq (y+1)^2</latex>
    //
    // the algorithm use the Newton method , i.e. it computes the sequence
    //
    //  <latex>x_{n+1}={1\over 2}(x_n+{x\over x_n})</latex>
    // until <latex>|x_{n+1}-x_n|\lneq 1</latex>
    //
    // Examples
    // 
    // sqrt(9876543210)
    // sqrt(bigint(9876543210))
    // 
    // See also
    //  bigint
    // sqrt
    // abs
    //
    // Authors
    //  Philippe Roux
    //

    // check x>0
    if x<0 then error(' square root of negative bigint not implemented!')
    elseif (type(x)==1 )|( x<1e15) then
        y=int(sqrt(iconvert(x,0)))// iconvert-> double ?scilab6 bug?
    else    //  Newton algorithm
        x0=x
        y=x0
        x=x0/2
        while abs(x-y)>1
            y=x
            x=div2(x+x0/x)
        end
        y=x//round below
        if y*y>x0 then y=y-1
        end
    end
endfunction
