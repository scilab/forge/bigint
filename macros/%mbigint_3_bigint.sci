function [bool]=%mbigint_3_bigint(A,x)
    // overloading compatibility for &#8804; (lower or equal) operator with mbigint/bigint
    // Calling Sequence
    //   bool=(A<=x)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // mbigint/bigint "lower or equal" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M<=x
    // N<=y
    // M<=z
    // 
    // See also
    //  mbigint
    // bigint_3_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    bool=(zeros(p,n)==0)
    for i=1:p
        for j=1:n
            bool(i,j)=(A.value{i,j}<=x)
        end
    end
endfunction
