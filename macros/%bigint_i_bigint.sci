function M=%bigint_i_bigint(varargin)
    // overloading compatibility for insertion operator () with bigint/bigint 
    // Calling Sequence
    //   M(i,j)=x
    //
    // Parameters
    // M : mbigint
    // i,j : integers
    // x : bigint
    //
    // Description
    // insert element x in the i-th line j-th column of bigint matrix M. you can use  vector notations for indexes as  M(a:b,c) M(a,b:c), M(a:b,c:d), M(a,:) M(:,b) M([...]) ....
    //
    // Examples
    // 
    // x=bigint(2)^57-1 
    // x(2:3,4:5)=bigint(2)
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    M=varargin($)  //bigint
    x=varargin($-1)// bigint
    // indexes of cell to be inserted
    B=0
    B(varargin(1:$-2))=1// a work around to manage index vs line/column
    [p,n]=size(B)
    if p*n>1 then
        [I,J]=find(B==1)
        // inserting new values
        D=string(zeros(p,n))
        D(1,1)=string(M)
        T=zeroscell(p,n)
        T{1,1}=M
        xstr=string(x)
        for k=1:length(I)
            i=I(k);j=J(k)
            D(i,j)=xstr
            T{i,j}=x
        end
        M=mlist(['mbigint','display','value'],D,T)
    else M=x

    end
endfunction
