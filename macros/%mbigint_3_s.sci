function [bool]=%mbigint_3_s(A,x)
    // overloading compatibility for &#8804; (lower or equal) operator with mbigint/double
    // Calling Sequence
    //   bool=(A<=x)
    // Parameters
    //  A: mbigint
    //  x : double
    //  bool : boolean matrix
    //
    // Description
    // mbigint/double  "lower or equal" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M<=123456789
    // M<=2345678
    // M<=-123456789
    // M<=[987654321 123; 0 -123]
    // 
    // See also
    //  mbigint
    // mbigint_3_bigint
    // mbigint_3_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    bool=(A<=x)
    endfunction
