function [B]=%s_d_mbigint(x,A)
    // overloading compatibility for binary operators ./ (element wise right divide) with double/mbigint
    // Calling Sequence
    //   B=x./A
    //
    // Parameters
    //  A,B: mbigint
    //    x: double
    //
    // Description
    // double/mbigint "element wise right divide divide" operator
    //
    // Examples
    // //
    // x=1234567890
    // y=bigint('987654321') 
    // M=[y x; -x -y]
    // N=[x x;x x]
    // x./M
    // N./M
    // 
    // See also
    //  mbigint
    // mbigint_d_mbigint
    // bigint_d_mbigint
    //
    // Authors
    //  Philippe Roux
    //


    x=mbigint(x) //  
     B=x./A
    endfunction
