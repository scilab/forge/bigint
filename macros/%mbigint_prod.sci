function [S]=%mbigint_prod(M,varargin)
    //  bigint matrix prod
    // Calling Sequence
    //   S=prod(M [,opt])
    //
    // Parameters
    //  M : mbigint
    //  S : bigint or mbigint
    // opt : string 'c' or 'r'
    //
    // Description
    // product of the bigint matrix entries, you can specify product over rows/column with optional argument 'r' ou 'c' .
    //
    // Examples
    // 
    // x=bigint(123456789);
    // y=bigint(987654321);
    // z=x-y;
    // L=[x y z]
    // prod(L) //= x*y*z
    // x*y*z
    //  M=[L; L]
    // prod(M)
    // prod(M,'r') // = [x^2 y^2 z^2]
    // prod(M,'c') // = [x*y*z;x*y*z]
    // 
    // See also
    //  size
    // matrix_bigint
    // mbigint
    // prod
    // mbigint_prod
    //
    // Authors
    //  Philippe Roux
    //
    
     [lhs,rhs]=argn(0);
     if rhs>1 then opt=varargin(1)
         else opt=''
     end
     [p,n]=size(M)
     select opt
     case 'c'
         S=[]
         for i=1:p
             temp=bigint(1)
             for j=1:n
                 temp=temp*M.value{i,j}
             end
         S=[S;temp]
         end
     case 'r'
         S=[]
         for j=1:n
             temp=bigint(1)
             for i=1:p
                 temp=temp*M.value{i,j}
             end
         S=[S temp]
         end
     else S=bigint(1)
         for i=1:p
             for j=1:n
                 S=S*M.value{i,j}
             end
         end 
     end
endfunction
