function [bool]=%bigint_4_s(x,y)
    // overloading  compatibility for &#8805;  operator (greater or equal) with bigint/double
    // Calling Sequence
    //       x>=y
    //   bool=(x>=y)
    //
    // Parameters
    // x : bigint
    // y : double(scalar)
    // bool : boolean
    //
    // Description
    // bigint/double   "greater or equal" operator
    //
    // Examples
    //
    // x=bigint('123456789')
    // y=1234567890
    // x>=y  //  false
    // y=12345678
    // x>=y // true
    // 
    // See also
    //  bigint
    // mbigint
    // bigint_4_mbigint
    // bigint_4_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert y to bigint
    y=mbigint(y)
    bool=(x>=y)
endfunction
