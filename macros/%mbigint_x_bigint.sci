function [B]=%mbigint_x_bigint(A,x)
    // overloading  compatibility for binary operators .* (element wise product) with mbigint/bigint
    // Calling Sequence
    //   B=A*x
    //
    // Parameters
    // A,B: bigint
    // x : double(scalar)
    //
    // Description
    // mbigint/bigint element wise product
    //
    // Examples
    // // 
    // x=123456
    // y=456789
    // z=987654
    // MM=[x y; z x-z]
    // M=mbigint(MM)
    // N=[x -z; -y x-z]
    // M*N
    // MM*N //double verification
    // P=[x x;x x]
    // N*P
    // N*bigint(x)
    // 
    // See also
    // mbigint
    // bigint_m_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    B=x*A
endfunction
