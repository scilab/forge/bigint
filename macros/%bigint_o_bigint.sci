function [bool]=%bigint_o_bigint(x,y)
    // binary operator == (equal) for bigint
    // Calling Sequence
    //   x==y
    //
    // Parameters
    //  x,y: bigint
    //  bool : boolean
    //
    // Description
    // bigint equal operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('0123456789') 
    // z=bigint('1234567890') 
    // x==y  // true 
    // x==z  // false
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    x=check_bigint(x)
    y=check_bigint(y)
    bool=(x.signe==y.signe)&(and(x.rep==y.rep))
endfunction
