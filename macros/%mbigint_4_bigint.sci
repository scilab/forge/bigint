function [bool]=%mbigint_4_bigint(A,x)
    // overloading compatibility for &#8805; (greater or equal) operator mbigint/bigint
    // Calling Sequence
    //   bool=(A>=x)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // mbigint/bigint "greather or equal" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M>=x
    // N>=y
    // M>=z
    // 
    // See also
    //  mbigint
    // bigint_4_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    bool=(zeros(p,n)==0)
    for i=1:p
        for j=1:n
            bool(i,j)=(A.value{i,j}>=x)
        end
    end
endfunction
