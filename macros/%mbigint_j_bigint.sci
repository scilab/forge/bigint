function [C]=%mbigint_j_bigint(A,B)
    // overloading compatibility for binary operators .&#94; (element wise power) with mbigint/bigint
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  A,C: mbigint
    //    B : bigint
    //
    // Description
    // mbigint/bigint "element wise power" operator
    //
    // Examples
    // //
    // x=bigint('12345') 
    // y=bigint('98765') 
    // M=[x y; y x]
    // N=bigint(3)
    // M.^N
    // y^3
    // x^3
    // 
    // See also
    //  mbigint
    // bigint_p_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    C=A
    for i=1:p
        for j=1:n
            a=A.value{i,j}
            c=(a^B)
            C.value{i,j}=c
            C.display(i,j)=string(c)
            //typeof(A.value(i,j))= ce(ll) 
            //typeof{A.value(i,j})= bigint
        end
    end
endfunction
