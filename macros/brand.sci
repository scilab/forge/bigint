function N=brand(varargin)
    // random bigint generator
    // Calling Sequence
    //   N=brand(Nmax)
    //   N=brand(Nmax,Nmin)
    //   N=brand(p,n,Nmax)
    //   N=brand(p,n,Nmax,Nmin)
    //
    // Parameters
    //  N,Nmax,Nmin: bigint
    //  p,n  : integers
    //
    // Description
    // random bigint generator with syntax :
    //<itemizedlist>
    // <listitem><para> N=brand(Nmax) produce a single bigint <latex>0\leq N\leq N_{max}</latex> </para></listitem>    
    // <listitem><para> N=brand(Nmax,Nmin) produce a single bigint <latex>N_{min}\leq N\leq N_{max}</latex> </para></listitem>
    // <listitem><para> N=brand(p,n,Nmax) produce a pxn mbigint matrix with <latex> 0\leq N(i,j)\leq N_{max},\,\forall i=1,\dots,p,j=1,\dots,n</latex> </para></listitem>
    // <listitem><para> N=brand(p,n,Nmax,Nmin) produce a pxn mbigint matrix with <latex> N_{min}\leq N(i,j)\leq N_{max},\,\forall i=1,\dots,p,j=1,\dots,n</latex> </para></listitem>
    // </itemizedlist>    
    // the function use grand(p,n,'uin',...) to generate independant blocks of 7 digits.
    //
    // Examples
    // 
    // a=bigint('123456789987654321')
    // b=bigint('100000000000000000')
    // x=brand(a)        // x<=a
    // x=brand(a,b)      // b<=x<=a
    // A=brand(2,3,a)    // A(i,j)<=a
    // B=brand(2,3,a,b)  // B(i,j)<= b
    // 
    // See also
    //  bigint
    // grand
    //
    // Authors
    //  Philippe Roux
    //

    [lhs,rhs]=argn(0)
    select  rhs
    case 0 , mprintf('brand syntax :\n x=brand(Nmax)          one value with           x  <=Nmax\n x=brand(Nmax,Nmin)     one value with  Nmin<=   x  <=Nmax\n M=brand(p,n,Nmax)      p*n matrix with       M(i,j)<=Nmax\n M=brand(p,n,Nmax,Nmin) p*n matrix with Nmin<=M(i,j)<=Nmax\n' )
        N=0; return
    case 1 , Nmax=bigint(varargin(1)),Nmin=bigint(0), p=1,n=1
    case 2 , Nmax=bigint(varargin(1)),Nmin=bigint(varargin(2)), p=1,n=1
    case 3 , Nmax=bigint(varargin(3)),Nmin=bigint(0), p=varargin(1),n=varargin(2)
    case 4 , Nmax=bigint(varargin(3)),Nmin=bigint(varargin(4)), p=varargin(1),n=varargin(2)
    else error('wrong number of arguments!')
    end
    if Nmin>Nmax then tmp=Nmin,Nmin=Nmax,Nmax=tmp
        warning(' Nmin>Nmax, interverting both values!')
    end
    // random generation  ou N with 0<= N <= Nmax-Nmin
    P=Nmax-Nmin    
    L=length(P.rep)
    N=mbigint(zeros(p,n))
    for i=1:p
        for j=1:n
            toobig=%T
            while toobig //generate N(i,j)<=P
                nstr='' 
                for k=1:L-1
                    nstr=string(grand(1,1,'uin',0,(1e+7)-1))+nstr
                end
                nstr=string(grand(1,1,'uin',0,P.rep($)))+nstr
                x=bigint(nstr)
                N(i,j)=x
                toobig=(N(i,j)>P)
            end
//            mprintf('N(%d,%d)=%s=\n',i,j,nstr),disp(N(i,j))
        end
    end
    N=N+Nmin // translate such that Nmin<= N<= Nmax
endfunction
