function [M]=%mbigint_round(M)
    // overloading compatibility "integer part" (round) for mbigint
    // Calling Sequence
    //   M=round(M)
    //
    // Parameters
    //  M: mbigint
    //
    // Description
    // integer part of mbigint
    //
    // Examples
    // //
    // M=brand(2,3,1e5) 
    // round(M)
    // round(-M)
    // 
    // See also
    //  mbigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
