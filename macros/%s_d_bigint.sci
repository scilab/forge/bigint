function [q]=%s_d_bigint(x,y)
    // overloading  compatibility for binary operator ./ (element wise right division) with double/bigint
    // Calling Sequence
    //   q=x./y
    //
    // Parameters
    //  y,q: bigint
    // x : scalar (double)
    //
    // Description
    // double/bigint "element wise right division" 
    //
    // Examples
    // 
    // x=98765432
    // y=bigint('12345678') 
    // q=x./y             //  bigint division
    // 98765432/12345678  // verify
    // (x*ones(2,2))./y   // matrix case
    // 
    // 
    // See also
    //  mbigint
    // bigint_d_bigint
    // mbigint_d_bigint 
    //
    // Authors
    //  Philippe Roux
    //
    
    x=mbigint(x)
    q=x./y
endfunction
