function [bool]=%mbigint_2_mbigint(A,B)
    // binary operators &#62; (greater) for mbigint
    // Calling Sequence
    //   bool=(A>B)
    //
    // Parameters
    //  A,B: mbigint
    //  bool : boolean
    //
    // Description
    // mbigint "greater" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x x; x x]
    // N=[x y; z x-z]
    // M>N
    // N>M
    // M>M
    // 
    // See also
    //  mbigint
    // mbigint_1_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(B<A)
    endfunction
