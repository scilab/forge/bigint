function [B]=%s_x_mbigint(x,A)
    // overloading compatibility for binary operator ".*" (element wise product) with double/mbigint
    // Calling Sequence
    //   B=x.*A
    //
    // Parameters
    // A,B: bigint
    // x : double
    //
    // Description
    // double/mbigint element wise product
    //
    // Examples
    // // 
    // x=123456
    // y=456789
    // z=987654
    // MM=[x y; z x-z]
    // M=mbigint(MM)
    // x.*M
    // x.*MM //double verification
    // P=[x x;x x]
    // P*N
    // bigint(x)*N
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    if typeof(x)=='bigint' then
        B=x*A
        else B=x.*A
    end
endfunction
