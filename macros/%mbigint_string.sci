function str=%mbigint_string(M)
    // bigint basics
    // Calling Sequence
    // [str]=string(M)
    //
    // Parameters
    // M: mbigint
    // str: string
    //
    // Description
    // convert mbigint to a string 
    //
    // Examples
    //
    // M=brand(2,3,1e+9)
    // string(M)
    //
    // See also
    // string
    // mbigint
    // mbigint_p
    //
    // Authors
    // Philippe Roux
    //

    str=M.display
endfunction
