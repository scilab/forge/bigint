function [B]=%s_m_mbigint(x,A)
    // overloading  compatibility for binary operators * (product) with double/mbigint
    // Calling Sequence
    //   B=x*A
    //
    // Parameters
    // A,B: bigint
    // x : double(scalar)
    //
    // Description
    // double/mbigint product
    //
    // Examples
    // // 
    // x=123456
    // y=456789
    // z=987654
    // M=[x y; z x-z]
    // NN=[x -z; -y x-z]
    // N=mbigint(NN)
    // M*N
    // MM*NN //double verification
    // P=[x x;x x]
    // P*N  // matrix case
    // P*NN
    // 
    // See also
    //  mbigint
    // bigint_m_mbigint
    // mbigint_m_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    B=x*A
endfunction
