function [y]=%bigint_s(x)
    // unary operator - (minus) for bigint
    // Calling Sequence
    //   y=-x
    //
    // Parameters
    //  x,y: bigint
    //
    // Description
    //  bigint opposite
    //
    // Examples
    // x=bigint('-1234567890') 
    // -x
    // // special case "-0=0"
    // x=bigint('0')
    // -x 
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    y=x
    y.signe=-1*y.signe
    y=check_bigint(y)
endfunction
