function [B]=%s_l_mbigint(x,A)
    // overloading compatibility for binary operators \ (divide) with double/mbigint
    // Calling Sequence
    //   B=x\A
    //
    // Parameters
    //  A,B: mbigint
    //    x: double
    //
    // Description
    // double/mbigint "divide" operator
    //
    // Examples
    // //
    // x=1234567890
    // y=bigint('987654321') 
    // M=[y x; -x -y]
    // N=[x x;x x]
    // x\M    // scalar case
    // N\M    // matrix case
    // 
    // See also
    //  mbigint
    // bigint_l_mbigint
    // mbigint_l_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    B=x\A
    endfunction
