function q=%s_l_bigint(x,y)
    // overloading compatibility binary operator \ (left divide) for double/bigint
    // Calling Sequence
    //   q=x\y
    //
    // Parameters
    //  y,q: bigint
    //    x: double
    //
    // Description
    // double/bigint "left divide"
    //
    // Examples
    // 
    // y=bigint('98765432')
    // x=12345678
    // q=x\y  //  bigint division
    // 98765432/12345678
    // r=y-q*x 
    // pmodulo(98765432,12345678) //  division with doubles
    // y== q*x+r
    // r<abs(x)
    // 
    // See also
    //  mbigint
    // bigint_l_bigint
    // mbigint_l,bigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    q=x\y
endfunction

