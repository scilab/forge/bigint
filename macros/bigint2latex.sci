function str=bigint2latex(x)
    // bigint basics
    // Calling Sequence
    // [str]=bigint2latex(x)
    //
    // Parameters
    // x: bigint
    // str: string
    //
    // Description
    // convert bigint to latex for compatibility with prettyprint 
    //
    // Examples
    //
    // x=bigint('-1234567890')
    // string(x)
    // bigint2latex(x)
    // prettyprint(x)
    //
    // See also
    // string
    // prettyprint
    //
    // Authors
    // Philippe Roux
    //

    str = '$'+string(x)+'$'
endfunction
