function [B]=%mbigint_m_bigint(A,x)
    // overloading compatibility for binary operators * (product) with mbigint/bigint
    // Calling Sequence
    //   B=A*x
    //
    // Parameters
    //  A,B: mbigint
    //    x: bigint
    //
    // Description
    // mbigint/bigint "product" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('9876543') 
    // M=[y x; -x -y]
    // N=[x x;x x]
    // M*x
    // M*N
    // 
    // See also
    //  mbigint
    // bigint_m_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    B=x*A
    endfunction
