function [k]=%bigint_length(x)
    // overloading compatibility for length with bigint
    // Calling Sequence
    //   k=length(x)
    //
    // Parameters
    //  x: bigint
    //  k : integer
    //
    // Description
    // length of bigint is 1 ( bigint considered as a mbigint with 1 cell)
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // k=length(x) // = 1
    // 
    // See also
    //  size
    //
    // Authors
    //  Philippe Roux
    //

    x=check_bigint(x)
    k=1
endfunction
