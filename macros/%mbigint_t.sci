function T=%mbigint_t(M)
    // transpose operator .' for mbigint 
    // Calling Sequence
    //   M'
    //
    // Parameters
    //  M,T : mbigint
    //
    // Description
    // transposition T(i,j)=M(j,i)
    //
    // Examples
    // 
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // M=[x y x-y],M=[M;M]
    // M'
    // 
    // See also
    //  mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
  T=mlist(['mbigint','display','value'],(M.display)',(M.value)')
endfunction
