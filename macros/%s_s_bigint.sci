function [z]=%s_s_bigint(x,y)
    // overloading compatibility for binary operator - (minus) with double/bigint
    // Calling Sequence
    //   z=x-y
    //
    // Parameters
    //  y,z: bigint
    // x : double(scalar)
    //
    // Description
    // double/bigint soustraction ("minus" operator)
    //
    // Examples
    // //
    // x=123456789
    // y=bigint('987654321') 
    // x-y  //  scalar/bigint soustraction
    // 123456789-987654321 // double soustraction
    // (x*ones(2,2))-y    //  matrix case
    // 
    // See also
    //  mbigint
    // bigint_s_bigint
    // mbigint_s_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    z=x-y
endfunction
