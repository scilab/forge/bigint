function [bool]=%bigint_2_bigint(x,y)
    // binary operator &#62; (greater) for bigint 
    // Calling Sequence
    //   bool=(x>y)
    //
    // Parameters
    //  x,y: bigint
    //  bool : boolean
    //
    // Description
    // bigint  greater  operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // x>y  // true 
    // x<z  // false
    // 
    // See also
    //  bigint
    // bigint_1_bigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(y<x)  // see bigint_1_bigint
    endfunction
