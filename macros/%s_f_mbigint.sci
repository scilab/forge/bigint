function M=%s_f_mbigint(x,L)
    // overloading compatibility for column concatenation with double/mbigint 
    // Calling Sequence
    //   M=[x;L]
    //
    // Parameters
    // M,L: mbigint
    // x : double
    //
    // Description
    // concatenate a double and a mbigint to a column  mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // L=[x;y]
    // M=[1;L]            // scalar case
    // L=[x y ; y x]
    // M= [zeros(2,2);L]  // matrix case
    // M=[ []; L]        // empty case
    // 
    // See also
    //  mbigint
    // mbigint_f_mbigint
    // bigint_f_mbigint
    //
    // Authors
    //  Philippe Roux
    //


    if x==[] then M=L
    else x=mbigint(x)
        M=[x;L]  
    end
endfunction
