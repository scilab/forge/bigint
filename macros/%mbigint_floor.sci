function [M]=%mbigint_floor(M)
    // overloading compatibility "integer part" (floor) for mbigint
    // Calling Sequence
    //   M=floor(M)
    //
    // Parameters
    //  M: mbigint
    //
    // Description
    // integer part of mbigint
    //
    // Examples
    // //
    // M=brand(2,3,1e5) 
    // floor(M)
    // floor(-M)
    // 
    // See also
    //  mbigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
