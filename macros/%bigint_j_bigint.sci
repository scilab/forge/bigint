function [C]=%bigint_j_bigint(A,B)
    // overloading compatibility for binary operators .&#94; (element wise power) for bigint
    //
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  A,B, C : bigint
    //
    // Description
    // bigint  "element wise power" operator
    //
    // Examples
    // //
    // x=bigint('45') 
    // y=bigint(10)
    // x.^y
    // 
    // See also
    //  mbigint
    // bigint_p_bigint
    //
    // Authors
    //  Philippe Roux
    //

    C=A^B
endfunction
