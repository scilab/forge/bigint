function [bool]=%s_o_bigint(x,y)
    // overloading  compatibility for binary operator "==" (equal)with double/bigint
    // Calling Sequence
    //   x==y
    //   bool=(x==y)
    //
    // Parameters
    // y : bigint
    // x : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint "equal" comparison
    //
    // Examples
    //
    // x=123456789
    // y=bigint('123456789') 
    // x==y  //  True
    // x=12345678
    // x==y // false
    // [123456789 12345678]==y // matrix case
    // 
    // See also
    //  mbigint
    // mbigint_o_bigint
    // bigint_o_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    bool=(x==y)
endfunction
