function [z]=%s_a_bigint(x,y)
    // overloading compatibility for binary operator + (plus) with double/bigint 
    // Calling Sequence
    //   z=x+y
    //
    // Parameters
    //  y,z: bigint
    // x : double(scalar)
    //
    // Description
    // double/bigint addition
    //
    // Examples
    // //
    // x=123456789
    // y=bigint('987654321') 
    // x+y                     // double/bigint addition
    // x+987654321             // double addition
    // (x*ones(2,2))+987654321 // matrix double/bigint addition
    // 
    // See also
    //  mbigint
    // bigint_a_bigint
    // mbigint_a_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    z=x+y
endfunction
