function [C]=%mbigint_a_mbigint(A,B)
    // binary operators + (plus) for mbigint
    // Calling Sequence
    //   C=A+B
    //
    // Parameters
    //  A,B,C: mbigint
    //
    // Description
    // mbigint "plus" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x x; x x]
    // N=[x y; z x-z]
    // M+N
    // 
    // See also
    //  mbigint
    // bigint_a_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    [p1,n1]=size(B)
    if (p<>p1)|(n<>n1) then
        error('incompatible size!')
    else
        C=A
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=B.value{i,j}
                c=(a+b)
                C(i,j)=c
            end
        end
    end
    endfunction
