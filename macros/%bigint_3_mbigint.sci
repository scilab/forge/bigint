function [bool]=%bigint_3_mbigint(x,A)
    // overloading compatibility for &#8804; (lower or equal) operator for bigint/mbigint
    // Calling Sequence
    //   bool=(x<=A)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // bigint/mbigint "lower or equal" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // x<=M
    // y<=M
    // z<=M
    // 
    // See also
    //  mbigint
    // mbigint_3_bigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(A>=x)
endfunction
