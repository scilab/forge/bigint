function [bool]=%mbigint_o_s(A,x)
    // overloading compatibility for == (equal) operator with mbigint/double
    // Calling Sequence
    //      A==x
    //   bool=(A==x)
    // Parameters
    //  A: mbigint
    //  x : double
    //  bool : boolean matrix
    //
    // Description
    // mbigint/double "equal"  comparison
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M==123456789
    // M==2345678
    // M==1234567890
    // M<>[123456789 123; 0 -123]
    // 
    // See also
    //  mbigint
    // mbigint_o_bigint
    // mbigint_o_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    bool=(A==x)
endfunction
