function M=%bigint_c_s(x,y)
    // overloading compatibility [,] right line concatenation for  bigint/double
    // Calling Sequence
    //   M=[x y]
    //
    // Parameters
    // x : bigint
    // y : double
    //
    // Description
    // concatenate a double and bigint to a line mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=1
    // L=[x y]
    // y=[1:10]   // matrix case
    // L=[x y]
    // y=[]        // empty case
    // L=[x y]
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    
    [p,n]=size(y)
    if y==[] then
        T=cell(1,1)
        T{1,1}=x
        M=mlist(['mbigint','display','value'],..
        string(x),T)
    elseif p*n==1 then 
        y=bigint(y)
        T=cell(1,2)
        T{1,1}=x
        T{1,2}=y
        M=mlist(['mbigint','display','value'],..
        [string(x) string(y) ],T)  
        else y=mbigint(y)
        T=cell(1,n+1)
        T{1,1}=x
        T(1,2:n+1)=y.value
        M=mlist(['mbigint','display','value'],..
        [ string(x) y.display],T) 
    end      
endfunction
