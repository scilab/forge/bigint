function M=%bigint_c_bigint(x,y)
    // overloading compatibility for [,] line concatenation for  bigint 
    // Calling Sequence
    //   M=[x y]
    //
    // Parameters
    //  M : mbigint
    // x,y : bigint
    //
    // Description
    // concate two bigint to create a line mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // M=[x y]
    // typeof(M)
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    T=cell(1,2)
    T{1,1}=x
    T{1,2}=y
    M=mlist(['mbigint','display','value'],..
      [string(x) string(y)],T)  
endfunction
