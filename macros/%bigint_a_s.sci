function [z]=%bigint_a_s(x,y)
    // overloading  compatibility  for + "plus" operator with bigint/double
    // Calling Sequence
    //   z=x+y
    //
    // Parameters
    //  x,z: bigint
    // y : double(scalar)
    //
    // Description
    // double/bigint addition, element wise addition for double matrix
    //
    // Examples
    // //
    // x=bigint('123456789')
    // y=987654321
    // x+y  //  scalar/bigint addition
    // 123456789+987654321 // double addition
    // 
    // See also
    //  bigint
    // bigint_a_bigint
    // bigint_a_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    y=mbigint(y)
    z=x+y
endfunction
