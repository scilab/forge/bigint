function [B]=%mbigint_m_s(A,x)
    // overloading  compatibility  for * (product) with mbigint/double
    // Calling Sequence
    //   B=A*x
    //
    // Parameters
    //  A,B: mbigint
    //  x: double(scalar)
    //
    // Description
    // mbigint/double product
    //
    // Examples
    // //
    // x=123456
    // y=456789
    // z=987654
    // MM=[x y; z x-z]
    // M=mbigint(MM)
    // NN=[x -z; -y x-z]
    // N=mbigint(NN)
    // M*NN
    // MM*NN //double verification
    // 
    // See also
    //  mbigint
    // mbigint_m_bigint
    // mbigint_m_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    B=A*x
endfunction
