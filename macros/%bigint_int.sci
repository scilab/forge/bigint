function [x]=%bigint_int(x)
    // overloading compatibility "integer part" (int) for bigint
    // Calling Sequence
    //   x=int(x)
    //
    // Parameters
    //  x: bigint
    //
    // Description
    // integer part of bigint
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // int(x)
    // int(-x)
    // 
    // See also
    //  bigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
