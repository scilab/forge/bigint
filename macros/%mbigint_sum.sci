function [S]=%mbigint_sum(M,varargin)
    //  bigint matrix sum
    // Calling Sequence
    //   S=sum(M [,opt])
    //
    // Parameters
    //  M : mbigint
    //  S : bigint or mbigint
    // opt : string 'c' or 'r'
    //
    // Description
    // sum of the bigint matrix entries, you can specify sum over rows/column with optional argument 'r' ou 'c' .
    //
    // Examples
    // 
    // x=bigint(123456789);
    // y=bigint(987654321);
    // z=x-y;
    // L=[x y z]
    // sum(L) //= 2*x
    // 2*x
    //  M=[L; L]
    // sum(M)
    // sum(M,'r') // = [2*x 2*y 2*z]
    // sum(M,'c') // = [2*x; 2*x]
    // 
    // See also
    //  size
    // matrix_bigint
    // mbigint
    // prod
    //
    // Authors
    //  Philippe Roux
    //
    
     [lhs,rhs]=argn(0);
     if rhs>1 then opt=varargin(1)
         else opt=''
     end
     [p,n]=size(M)
     select opt
     case 'c'
         S=[]
         for i=1:p
             temp=bigint(0)
             for j=1:n
                 temp=temp+M.value{i,j}
             end
         S=[S;temp]
         end
     case 'r'
         S=[]
         for j=1:n
             temp=bigint(0)
             for i=1:p
                 temp=temp+M.value{i,j}
             end
         S=[S temp]
         end
     else S=bigint(0)
         for i=1:p
             for j=1:n
                 S=S+M.value{i,j}
             end
         end 
     end
endfunction
