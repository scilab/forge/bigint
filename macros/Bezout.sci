function [varargout]=Bezout(x,y)
    // bigint  Bezout identity
    // Calling Sequence
    //   [d,a,b]=Bezout(x,y)
    //
    // Parameters
    //  x,y,d,a,b: bigint
    //
    // Description
    // Bezout identity for bigint using Euclide algorithm 
    //  <latex>d= a\times x+b\times y</latex>
    //
    // Examples
    // 
    // x=bigint('98765432') 
    // y=bigint('12345678') 
    // [d,a,b]=Bezout(x,y) 
    // a*x+b*y==d
    // x=bigint('123456789987654321')
    // y=bigint('987654321123456789')
    // [d,a,b]=Bezout(x,y) 
    // a*x+b*y==d
    // 
    // See also
    //  bigint 
    //  divide
    //  PGCD
    //
    // Authors
    //  Philippe Roux
    //

    [lhs,rhs]=argn(0)
    // convert to bigint if necessary
    if type(x)==1 then x=bigint(x)
    end
    if type(y)==1 then y=bigint(y)
    end
    // check division by 0
    sign_x=sign(x);
    x=abs(x),
    sign_y=sign(y);
    y=abs(y)
    if y==0 then // d=1*x+0*y
        d=x
        a=bigint(1)
        b=bigint(0)
    else // reduce to x,y>0 
        [q,r]=divide(x,y)
        [d,u,v]=Bezout(y,r)
        //d=y*u+r*v=y*u+(x-y*q)*v=y*(u-q*v)+x*v
        a=v*sign_x
        b=(u-q*v)*sign_y
    end
    //format output
    select lhs
    case 1 
        varargout=list([d,a,b])
    case 3
        varargout=list(d,a,b)
    else 
        error('wrong number of output arguments!')
    end
endfunction
