function [B]=%mbigint_s_s(A,x)
    // overloading compatibility for binary operator - (minus) with mbigint/double
    // Calling Sequence
    //   B=A-x
    //
    // Parameters
    //  A,B: mbigint
    //  x : double
    //
    // Description
    // mbigint/double "minus" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M-123456789
    // M-123456789*ones(2,2)
    // 
    // See also
    //  mbigint
    // mbigint_s_bigint
    // mbigint_s_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    B=A+(-x)
    endfunction
