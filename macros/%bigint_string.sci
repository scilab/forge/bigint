function str=%bigint_string(x)
    // bigint basics
    // Calling Sequence
    // [str]=string(x)
    //
    // Parameters
    // x: bigint
    // str: string
    //
    // Description
    // convert bigint to a string 
    //
    // Examples
    //
    // x=bigint('-1234567890')
    // string(x)
    //
    // See also
    // string
    // bigint
    // bigint_p
    //
    // Authors
    // Philippe Roux
    //

    str = ''
    // digits
    n = length(x.rep)
    for k = 1:n,
        bloc = string(x.rep(k))
        while (length(bloc)<7) & (k<n) 
            bloc = '0' + bloc
        end,
        str = bloc + str
    end,
    //sign
    if x.signe == (-1) then
        str = '-' + str
    end
endfunction
