function [bool]=%mbigint_1_s(A,x)
    // overloading compatibility for &#60; (lower) operator with mbigint/double
    // Calling Sequence
    //   bool=(A<x)
    // Parameters
    //  A: mbigint
    //  x : double
    //  bool : boolean matrix
    //
    // Description
    // mbigint/double  "lower" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M<123456789
    // M<2345678
    // M<-123456789
    // M<[987654321 123; 0 -123]
    // 
    // See also
    //  mbigint
    // mbigint_1_bigint
    // mbigint_1_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    bool=(A<x)
    endfunction
