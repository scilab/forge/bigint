function [x]=%bigint_iconvert(x,itype)
    // convert mbigint to double
    // Calling Sequence
    //   y=double(x)
    //
    // Parameters
    //       x: mbigint
    //      y : double
    //  itype : not used
    //
    // Description
    // convert mbigint to double using string representation (with rounding errors if greather than <latex>10^{16}</latex>
    //
    // Examples
    // //
    // x=brand(1e10) 
    // typeof(x)
    // y=double(x)
    // typeof(y)
    // x=brand(1e20) // rounding error
    // y=double(x) 
    // 
    // See also
    //  bigint
    //  double
    //  bigint_string
    //
    // Authors
    //  Philippe Roux
    //

x=evstr(string(x))
if or(x>1e15) then warning('Probable rouding errors!')
end
endfunction
