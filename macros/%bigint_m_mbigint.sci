function [B]=%bigint_m_mbigint(x,A)
    // overloading compatibility for  operator * (product) with bigint/mbigint
    // Calling Sequence
    //   B=x*A
    //
    // Parameters
    //  A,B: mbigint
    //    x: bigint
    //
    // Description
    // bigint/mbigint "product" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('9876543') 
    // M=[y x; -x -y]
    // N=[x x;x x]
    // x*M
    // N*M
    // 
    // See also
    //  mbigint
    // bigint_m_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    if p*n==1 then
        B=x*(A.value{1,1})
    else
        D=string(zeros(p,n))
        T=cell(p,n)
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=(x*a)
                T{i,j}=b
                D(i,j)=string(b)
            end
        end
        B=mlist(['mbigint','display','value'],D,T)
    end
    endfunction
