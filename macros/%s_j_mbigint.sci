function [C]=%s_j_mbigint(A,B)
    // overloading compatibility for binary operators .&#94; (element wise power) with double/mbigint
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  B,C: mbigint
    //    A : double
    //
    // Description
    // double/mbigint "element wise power" operator
    //
    // Examples
    // //
    // x=12345
    // M=mbigint([0 1; 2 3])
    // x.^N
    // x^2
    // x^3
    // M.^mbigint(M)
    // 
    // See also
    //  mbigint
    // mbigint_j_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    A=bigint(A)
    C=A.^B
endfunction
