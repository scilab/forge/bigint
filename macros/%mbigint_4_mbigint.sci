function [bool]=%mbigint_4_mbigint(A,B)
    // binary operators &#8805; (greater or equal) for mbigint
    // Calling Sequence
    //   bool=(A>=B)
    //
    // Parameters
    //  A,B: mbigint
    //  bool : boolean
    //
    // Description
    // mbigint "greater or equal" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x x; x x]
    // N=[x y; z x-z]
    // M>=N
    // N>=M
    // M>=M
    // 
    // See also
    //  bigint
    // mbigint_3_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(B<=A)
    endfunction
