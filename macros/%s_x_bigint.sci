function [z]=%s_x_bigint(x,y)
    // overloading compatibility for binary operator ".*" (element wise product) with double/bigint
    // Calling Sequence
    //   z=x.*y
    //
    // Parameters
    //  y,z: bigint
    // x : double(scalar)
    //
    // Description
    // double/bigint element wise product
    //
    // Examples
    // 
    // x=12345678
    // y=bigint('98765432') 
    // x*y  //  scalar/bigint product
    // 12345678*98765432 // double product
    // x.*y // no type error
    // (x*ones(2,2)).*y  // matrix case
    // 
    // See also
    //  mbigint
    // bigint_x_bigint
    // mbigint_x_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    z=x*y
endfunction
