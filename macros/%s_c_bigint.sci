function M=%s_c_bigint(x,y)
    //  overloading compatibility for line concatenation with  double/bigint  
    // Calling Sequence
    //   M=[x y]
    //
    // Parameters
    // y : bigint
    // x : double
    //
    // Description
    // concatenate a double and a bigint to a line mbigint 
    //
    // Examples
    // //
    // y=bigint('123456789') 
    // x=1
    // M=[x y]
    // y=[1:10]   // matrix case
    // L=[x y]
    // y=[]        // empty case
    // L=[x y]
    // 
    // See also
    //  mbigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(x)
    if x==[] then
        T=cell(1,1)
        T{1,1}=y
        M=mlist(['mbigint','display','value'],..
        string(y),T)
    elseif p*n==1 then 
        x=bigint(x)
        T=cell(1,2)
        T{1,1}=x
        T{1,2}=y
        M=mlist(['mbigint','display','value'],..
        [string(x) string(y) ],T)  
        else x=mbigint(x)
        T=cell(1,n+1)
        T(1,1:n)=x.value
        T{1,n+1}=y
        M=mlist(['mbigint','display','value'],..
        [x.display string(y) ],T) 
    end
endfunction
