function q=%bigint_l_s(x,y)
    // overloading compatibility for operator \ (left divide) with bigint\double
    // Calling Sequence
    //   q=x\y
    //
    // Parameters
    //  x,q: bigint
    //    y: double
    //
    // Description
    // bigint/double "left divide" operator
    //
    // Examples
    // 
    // y=98765432
    // x=bigint('12345678') 
    // q=x\y  //  bigint division
    // 98765432/12345678
    // r=y-q*x 
    // pmodulo(98765432,12345678) //  division with doubles
    // y== q*x+r
    // r<abs(x)
    // 
    // See also
    //  mbigint
    // bigint_l_bigint
    // bigint_l_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    y=mbigint(y)
    q=x\y
endfunction

