function [B]=%bigint_a_mbigint(x,A)
    // overloading compatibility for binary operator + (plus) for bigint/mbigint
    // Calling Sequence
    //   B=x+A
    //
    // Parameters
    //  A,B: mbigint
    //  x : bigint
    //
    // Description)
    // bigint/mbigint "plus" operator, element wise addition
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // x+M
    // 
    // See also
    //  mbigint
    // mbigint_a_bigint
    //
    // Authors
    //  Philippe Roux
    //

    B=A+x
    endfunction
