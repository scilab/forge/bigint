function M=%mbigint_i_bigint(varargin)
    // overloading compatibility for insertion operator () with bigint 
    // Calling Sequence
    //   M(i,j)=x
    //
    // Parameters
    // M : mbigint
    // i,j : integers
    // x : bigint
    //
    // Description
    // insert element x in the i-th line j-th column of bigint matrix M. you can use  vector notations for indexes as  M(a:b,c) M(a,b:c), M(a:b,c:d), M(a,:) M(:,b) M([...]) ....
    //
    // Examples
    // 
    // x=bigint(2)^57-1 
    // x(2:3,4:6)=mbigint(ones(2,3))
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=varargin($)  //bigint
    A=varargin($-1)// mbigint
    [p,n]=size(A)
    // indexes of cell to be inserted
    B=zeros(p,n)
    B(varargin(1:$-2))=1// a work around to manage index vs line/column
    [I,J]=find(B==1)
    p1=length(unique(I))
    n1=length(unique(J))
    [p2,n2]=size(A)
    //printf('taille B=%dx%d, taille A= %d x %d\n', p1,n1,p2,n2)
    if (p1<>p2)|(n1<>n2) then
        error('Invalid index!')
    elseif p*n>1 then
        M=mbigint(B)
        M(1,1)=x// initial bigint value
        M(varargin(1:$-2))=A
    else M=x
    end
endfunction
