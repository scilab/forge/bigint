function [B]=%mbigint_a_bigint(A,x)
    // overloading compatibility for binary operator + (plus) for mbigint/bigint
    // Calling Sequence
    //   B=A+x
    //
    // Parameters
    //  A,B: mbigint
    //  x : bigint
    //
    // Description
    // mbigint/bigint "plus" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M+x
    // 
    // See also
    //  mbigint
    // bigint_a_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    if p*n==1 then
        B=x+(A.value{1,1})
    else
        D=string(zeros(p,n))
        T=cell(p,n)
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=(x+a)
                T{i,j}=b
                D(i,j)=string(b)
            end
        end
        B=mlist(['mbigint','display','value'],D,T)
    end
    endfunction
