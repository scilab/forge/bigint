function q=%mbigint_q_s(x,y)
    // overloading compatibility binary operator .\ (element wise left divide) for mbigint\double
    // Calling Sequence
    //   q=x.\y
    //
    // Parameters
    //  x,q: mbigint
    //    y: double
    //
    // Description
    //mbigint\double  "element wise left divide"
    //
    // Examples
    // 
    // x=123456789
    // xx=bigint(x) 
    // y=9876543
    // yy=bigint(y) 
    // x*y
    // M=[x y; -x -x]
    // N=[yy xx; yy -yy]
    // N.\M
    //
    // See also
    //  bigint
    // mbigint_q_mbigint
    // mbigint_q_bigint
    //
    // Authors
    //  Philippe Roux
    //

    y=mbigint(y)
    q=x.\y
endfunction
