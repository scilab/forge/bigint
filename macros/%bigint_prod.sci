function [M]=%bigint_prod(M,varargin)
    //  overloading compatibility for bigint matrix prod
    // Calling Sequence
    //   M=prod(M [,opt])
    //
    // Parameters
    //  M : mbigint
    // opt : string 'c' or 'r'
    //
    // Description
    // product of the bigint matrix entries (M(1,1) here) you can specify product over rows/column with optional argument 'r' ou 'c' .
    //
    // Examples
    // 
    // M=bigint(123456789);
    // prod(M)
    // prod(M,'r')
    // prod(M,'c') 
    // 
    // See also
    //  size
    // bigint
    // mbigint_prod
    // prod
    //
    // Authors
    //  Philippe Roux
    //
    
     //nothing to do !!!
endfunction
