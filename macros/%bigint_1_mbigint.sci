function [bool]=%bigint_1_mbigint(x,A)
    // overloading compatibility for &#60; (lower) operator with bigint/mbigint
    // Calling Sequence
    //   bool=(x<A)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // bigint/mbigint  &#60; "lower" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // x<M
    // y<M
    // z<M
    // 
    // See also
    //  bigint
    // mbigint
    // mbigint_1_bigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(A>x)
endfunction
