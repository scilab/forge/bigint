function x=bigint(str)
    // bigint basics
    // Calling Sequence
    // [x]=bigint(str)
    //
    // Parameters
    // x: bigint
    // str: string
    //
    // Description
    // create a "big integer" from its decimal representation. A bigint is a tlist with two fields
    //
    // 
    //    rep : a nx1 matrix such that rep(k) is a 7 digits integer (ie in the interval [0,10^7[)   
    //    signe = +1 ou -1   
    //    str is the decimal represention of  <latex> x= signe \times \sum_{k=1}^{n} rep(k) \times 10^{k-1}    </latex>
    //  
    //
    // Examples
    // //a 10 digit integer
    // bigint 1234567890987654321 // big int stored in ans
    // x=ans
    // x=bigint('-1234567890987654321') // use quote syntax for strings
    // x=bigint('00000000000000000') //=0
    // x=bigint('-0') // =0
    // x=bigint(123456789)  // double<10^16 or uint* to bigint convertion
    // bigint(-123456789987654321) // probable rounding error
    //
    // See also
    // mbigint
    // contribute
    //
    // Authors
    // Philippe Roux
    //

    if typeof(str)=='bigint' then
        x=str // nothing to do !
    else  //str can be a matrix of double/strings
        [p,n]=size(str);
        if (p>1)|(n>1) then
            x=mbigint(str)
        else str_is_inf=%F
            if (type(str) == 1)|(type(str)==8) then
                if isinf(str) then //%inf is
                    str_is_inf=%T
                    x_inf=str // store x value= + -%inf
                elseif abs(str)>1e+15 then
                    warning(['probable rounding error in bigint conversion'; 'prefer bigint(''123456789'') to bigint(123456789)'])
                elseif round(str)<>clean(str) then // clean--> to avoid non-significant warnings
                    warning(['non-integer entry !'])
                end
                str=clean(str)
                // str=double or uint8,16,32
                //  convert to string
                str=stripblanks(msprintf('%15.0f',str)) // becareful of white space before "-" sign
            end
            if str_is_inf then x=x_inf
            else // convert string to bigint
                str=stripblanks(str)
                // sign
                c = part(str, 1)
                signe = 1
                if c == '-' then
                    signe = -1
                    str = part(str, 2:$)
                end,
                // digits
                n = length(str)
                str = ascii(str)
                for k = 1:n/7,
                    tab(k) = evstr(ascii(str(n - k * 7 + 1:n - (k - 1) * 7)))
                end,
                tab($ + 1) = evstr(ascii(str(1:pmodulo(n, 7))))
                x = mlist(['bigint','signe','rep'], signe, tab)
                x = check_bigint(x)
            end
        end
    end
endfunction
