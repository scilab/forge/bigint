function [B]=%mbigint_x_s(A,x)
    // overloading  compatibility for binary operators * (element wise product) with double/mbigint
    // Calling Sequence
    //   B=A*x
    //
    // Parameters
    // A,B: bigint
    // x : double(scalar)
    //
    // Description
    // mbigint/double element wise  product
    //
    // Examples
    // // 
    // x=123456
    // y=456789
    // z=987654
    // MM=[x y; z x-z]
    // M=mbigint(MM)
    // N=[x -z; -y x-z]
    // M*N
    // MM*N //double verification
    // P=[x x;x x]
    // N*P
    // N*bigint(x)
    // 
    // See also
    //  mbigint
    // mbigint_m_mbigint
    //  mbigint_m_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    if typeof(x)=='bigint' then
        B=x*A
        else B=x.*A
    end
endfunction
