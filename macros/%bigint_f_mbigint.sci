function M=%bigint_f_mbigint(x,L)
    // overloading compatibility for column concatenation [;] for  bigint/mbigint
    // Calling Sequence
    //   M=[x;L]
    //
    // Parameters
    // M,L: mbigint
    // x : bigint
    //
    // Description
    // concatenate a bigint and mbigint to a column mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // L=[x;y]
    // M=[y-x;L]
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    [p,n]=size(L)
    T=cell(p+1,n)
    T{1,1}=x
    T(2:p+1,1)=L.value
    M=mlist(['mbigint','display','value'],..
      [string(x);L.display],T)  
endfunction
