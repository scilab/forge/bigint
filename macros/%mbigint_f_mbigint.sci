function M=%mbigint_f_mbigint(A,B)
    // column concatenation for  mbigint 
    // Calling Sequence
    //   M=[A;B]
    //
    // Parameters
    //  M,A,B : mbigint
    //
    // Description
    // if A and B are bigint matrix of size p1xn ans p2xn then M is a bigint matrix of size (p1+p2xn), otherwise produce error 5.
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // A=[x y]
    // B=[y x]
    // M=[A;B]
    // 
    // See also
    //  mbigint
    // mbigint_c_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
      M=mlist(['mbigint','display','value'],..
      [A.display;B.display],..
      [A.value;B.value])  
endfunction
