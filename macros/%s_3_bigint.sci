function [bool]=%s_3_bigint(x,y)
    // overloading  compatibility for &#8804; (lower or equal) operator with double/bigint
    // Calling Sequence
    //   x<=y
    //   bool=(x<=y)
    //
    // Parameters
    // y : bigint
    // x : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint "lower or equal" operator
    //
    // Examples
    //
    // x=12345678
    // y=bigint('123456789')
    // x<=y  //  True
    // x=1234567890
    // x<=y // false
    // (x*ones(2,2))<=y  // matrix case
    // 
    // See also
    //  mbigint
    // bigint_3_bigint
    // mbigint_3_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    bool=(x<=y)
endfunction
