function [bool]=%mbigint_n_mbigint(A,B)
    // binary " &#60;&#62;" (not equal) operator for mbigint
    // Calling Sequence
    //   A<>B
    //
    // Parameters
    //  A,B: mbigint
    //  bool : boolean
    //
    // Description
    // mbigint "not equal" comparison
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('0123456789') 
    // A=[x y]
    // B=[y x]
    // C=[x x]
    // A<>A  // true 
    // A<>B  // false
    // A<>C
    // 
    // See also
    //  mbigint
    // mbigint_o_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    bool=~(A==B)
endfunction
