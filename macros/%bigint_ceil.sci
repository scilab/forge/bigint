function [x]=%bigint_ceil(x)
    // overloading compatibility "integer part" (ceil) for bigint
    // Calling Sequence
    //   x=ceil(x)
    //
    // Parameters
    //  x: bigint
    //
    // Description
    // integer part of bigint
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // ceil(x)
    // ceil(-x)
    // 
    // See also
    //  bigint
    // int
    // floor
    //  ceil
    // round
    //
    // Authors
    //  Philippe Roux
    //

    //nothing to do !!!
endfunction
