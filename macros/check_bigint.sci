function x=check_bigint(x)
    // bigint basics
    // Calling Sequence
    // [x]=check_bigint(x)
    //
    // Parameters
    // x: bigint
    //
    // Description
    // check bigint structure, if not returns error. Always ends yours macros returning a "bigint" z  with the command z=check_bigint(z) this will return a normalized form for z.
    //
    // Examples
    //
    // x=bigint('-1234567890')
    // x=check_bigint(x) // OK
    // x.signe=%pi
    // x=check_bigint(x) // error
    // x=bigint('0')
    // x.signe=-1
    // x=check_bigint(x) // change signe to +
    //
    // See also
    // bigint
    // check_mbigint
    //
    // Authors
    // Philippe Roux
    //

    [lhs,rhs] = argn(0);
    // check fields
    if ~((typeof(x)=='bigint' )&and(fieldnames(x)== ['signe';'rep'])) then
        error('not bigint structure')
    end
    //check sign
    if abs(x.signe) ~= 1 then
        error('bigint signe field should be +1 or -1')
    end,
    //check digit
    if ~((type(x.rep) == 1) && (and(x.rep >= 0) & and(x.rep<(10^7)))) then
        error('bigint rep should be a vector of 7 digits positives integers')
    end,
    //special case "-0"
    if and(x.rep == 0) then x.signe = 1
    end,
    // too much 0 begining
    while x.rep($) == 0 then
        if length(x.rep) > 1 then
            x.rep($) = []
        else break,
        end,
    end,
    x = mlist(['bigint','signe','rep'], x.signe, x.rep)
endfunction
