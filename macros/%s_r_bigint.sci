function [q]=%s_r_bigint(x,y)
    //  overloading compatibility for binary operator / (right divide) with double/bigint
    // Calling Sequence
    //   q=x/y
    //
    // Parameters
    //  y,q: bigint
    // x : scalar (double)
    //
    // Description
    // double/bigint "right divide" operator
    //
    // Examples
    // 
    // x=98765432
    // y=bigint('12345678') 
    // q=x/y  //  bigint division
    // 98765432/12345678
    // (x*ones(2,2))/y  // matrix case
    // 
    // See also
    //  mbigint
    // biginr_r_mbigint
    // mbigint_r_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    x=mbigint(x)
    q=x/y
endfunction
