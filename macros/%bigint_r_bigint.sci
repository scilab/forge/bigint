function q=%bigint_r_bigint(x,y)
    // binary operator / (right division ) for bigint
    // Calling Sequence
    //   q=x/y
    //
    // Parameters
    //  x,y,q: bigint
    //
    // Description
    // bigint right division reduce to macro divide .
    //
    // Examples
    // 
    // x=bigint('98765432') 
    // y=bigint('12345678') 
    // q=x/y  //  bigint division
    // 98765432/12345678
    // r=x-q*y 
    // pmodulo(98765432,12345678) // double product
    // y=bigint('1234567890')
    // x=bigint('987654321123456789')
    // q=x/y
    // r=x-q*y
    // x== q*y+r
    // r<abs(y)
    // 
    // See also
    //  bigint
    // divide
    //
    // Authors
    //  Philippe Roux
    //

    [q,r]=divide(x,y)
endfunction

