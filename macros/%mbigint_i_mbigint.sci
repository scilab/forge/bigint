function M=%mbigint_i_mbigint(varargin)
    // insertion operator for mbigint matrix
    // Calling Sequence
    //   M(i,j)=N
    //
    // Parameters
    //  M,N : mbigint
    //  i,j : integers
    //
    // Description
    // insert element x in the i-th line j-th column of mbigint matrix M
    //
    // Examples
    // 
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // M=[x y x-y bigint(0)]
    // M(2,2)=x//  change cell (2,2)
    // M(1,1:2)=y  // change line 1
    // M(1:2,2)=-x //change column 2
    // M(:,1)=y     // short cuts
    // 
    // See also
    //  mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    M=varargin($)
    N=varargin($-1)
    // indexes of cell to be inserted
    [p,n]=size(M)
    B=zeros(p,n)
    B(varargin(1:$-2))=1// a work around to manage index vs line/column
    [I,J]=find(B==1)
    ind=find(B==1)
    [p1,n1]=size(N)
    B(ind)=1:p1*n1 // such that M(i,j)=N(B(i,j))

    // inserting new values
    T=M.value
    D=M.display
    for k=1:length(I)
        i=I(k);j=J(k)
        D(i,j)=N.display(k)
        T{i,j}=N.value{k}
    end
    M.value=T
    M.display=D
endfunction
