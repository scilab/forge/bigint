function M=%mbigint_f_bigint(L,x)
    // column concatenation for  mbigint/bigint overloading compatibility 
    // Calling Sequence
    //   M=[L;x]
    //
    // Parameters
    //  M,L: mbigint
    // x : bigint
    //
    // Description
    // concatenate a mbigint  with a bigint on its right to a column  mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // L=[x;y]
    // M=[L;y-x]
    // 
    // See also
    //  mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    [p,n]=size(L)
    T=cell(p+1,n)
    T{p+1,1}=x
    T(1:p,1)=L.value
    M=mlist(['mbigint','display','value'],..
      [L.display;string(x) ],T)  
endfunction
