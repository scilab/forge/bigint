function [C]=%s_j_bigint(A,B)
    // overloading compatibility for binary operators .&#94; (element wise power) with double/mbigint
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  A : double
    //  B : bigint
    //  C : mbigint
    //
    // Description
    // double/bigint "element wise power" operator
    //
    // Examples
    // //
    // x=bigint('45') 
    // M=[0 1 ; 2 3]
    // M.^x
    // 2^x
    // 3^x
    // 
    // See also
    //  mbigint
    // mbigint_j_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    A=mbigint(A)
    C=A.^B
endfunction
