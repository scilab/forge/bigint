function [bool]=%bigint_4_mbigint(x,A)
    // overloading compatibility for &#8805; (greater or equal) operator with bigint/mbigint 
    // Calling Sequence
    //   bool=(x>=A)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // bigint/mbigint "lower or equal" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // x>=M
    // y>=N
    // z>=M
    // 
    // See also
    //  bigint
    // mbigint
    // mbigint_3_bigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(A<=x)
endfunction
