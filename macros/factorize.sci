function [str,P,A]=factorize(n)
    // bigint factorization
    // Calling Sequence
    //  str=factorize(n)
    //  [str,P]=factorize(n)
    //  [str,P,A]=factorize(n)
    //
    // Parameters
    //   n : bigint
    // P,A : mbigint 
    // str : string 
    //
    // Description
    // bigint integer factorization by trivial division
    //<itemizedlist>
    // <listitem> 
    // <para>P is the list of prime factors <latex> p_j</latex></para>
    // </listitem> 
    // <listitem> 
    // <para>A is the list of prime factors exponents <latex> {a_j}</latex></para>
    // </listitem> 
    // <listitem> 
    // <para>str is a string representing the decomposition <latex> n= \Pi_{j=1}^k p_j^{a_j}=p_1^{a_1}\times ...\times p_k^{a_k}</latex></para>
    // </listitem> 
    // </itemizedlist> 
    // 
    //
    // Examples
    // 
    // n=bigint(2^3*5*17^4)
    // str=factorize(n)
    // evstr(str)==n           // verify
    // [str,P,A]=factorize(n)  // retrive factors and exponents
    // factorize(43691)        // a prime number
    // n=prod(primes(17))*prod(primes(9))*bigint(1e8)
    // factorize(n)       // = 2^(10)*3^(2)*5^(10)*7^(2)*11*13*17
    //  // stop search when p^2 >n ( below last prime tested p=223~ sqrt(51061)  )
    //  n=(bigint(2)^53)*bigint(51061)
    // factorize(n) // =2^(53)*51061 
    // 
    // See also
    //  bigint
    // factor
    // millerrabin
    //
    // Authors
    //  Philippe Roux
    //

    //  for "small" integers factorize use factor (increase speed) 
    if n<1e15 then 
	n=iconvert(n,0)// iconvert -> double ?scilab6 bug?
        L=factor(n)
        P=unique(L)
        A=P
        for k=1:length(P)
            A(k)=length(find(L==P(k)))
        end
        str=strcat(string(P)+'^('+string(A)+')','*')
    else  // convert to bigint if necessary
        if type(n)==1 then n=bigint(n) // rounding error ?
        end
        //initialization
        k=1;
        P=[];
        A=[];
        str=' '
        star=' '
        //case p=2
        p=bigint(2);
        a=bigint(0)
        while odd(n)==0
            n=div2(n)
            a=a+1
        end
        if a>0 then
            P=[p]
            A=[a]
            k=2
        end
        // odd  factors
        p=bigint(3)
        while (p*p<=n)
            a=bigint(0)
            [q,r]=divide(n,p)
            while r==0
                a=a+1;
                n=q
                [q,r]=divide(n,p)
            end
            if a>0 then //store factor and exponent 
                P=[P,p];
                A=[A,a];
                k=k+1;
            end
        p=p+2;
        end
        // du to test p*p<=n
        if n<>1 then //if n is prime no factor has been found
            P=[P,n];
            A=[A,1];
        end
    end
    // convert result to string 
    T=string(P)
    for k=1:length(P)
        if A(k)>1 then 
            T(k)=T(k)+'^('+string(A(k))+')'
        end
    end
    str=strcat(T,'*')
endfunction
