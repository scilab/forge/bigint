function [bool]=%mbigint_4_s(A,x)
    // overloading  compatibility for &#8805;  operator (greater or equal) with mbigint/double
    // Calling Sequence
    //       A>=x
    //   bool=(A>=x)
    //
    // Parameters
    // A : mbigint
    // x : double(scalar)
    // bool : boolean
    //
    // Description
    // mbigint/double "greater or equal" operator
    //
    // Examples
    //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M>=123456789
    // M>=2345678
    // M>=-123456789
    // M>=[987654321 123; 0 -123]
    // 
    // See also
    //  bigint
    // bigint_3_mbigint
    // mbigint_3_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert y to bigint
    x=mbigint(x)
    bool=(x<=A)
endfunction
