function M=%s_f_bigint(x,y)
    // left column concatenation for  bigint matrix/ overloading compatibility 
    // Calling Sequence
    //   M=[x;y]
    //
    // Parameters
    // y : bigint
    // x : double
    //
    // Description
    // concatenate a double to a line mbigint 
    //
    // Examples
    // //
    // x=1
    // y=bigint('123456789') 
    // L=[x;y]
    // x=[1:10]'   // matrix case
    // L=[x;y]
    // x=[]        // empty case
    // L=[x;y]
    // See also
    //  mbigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(x)
    if x==[] then
        T=cell(1,1)
        T{1,1}=y
        M=mlist(['mbigint','display','value'],..
        string(y),T)
    elseif p*n==1 then 
        x=bigint(x)
        T=cell(1,2)
        T{1,1}=x
        T{2,1}=y
        M=mlist(['mbigint','display','value'],..
        [string(x);string(y) ],T)  
        else x=mbigint(x)
        T=cell(n+1,1)
        T(1:n,1)=x.value
        T{n+1,1}=y
        M=mlist(['mbigint','display','value'],..
        [x.display;string(y) ],T) 
    end
endfunction
