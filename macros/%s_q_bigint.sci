function q=%s_q_bigint(x,y)
    // overloading compatibility binary operator .\ (element wise left divide) with double/bigint
    // Calling Sequence
    //   q=x.\y
    //
    // Parameters
    //  y,q: bigint
    //    x: double
    //
    // Description
    // double/bigint element wise left divide
    //
    // Examples
    // 
    // y=bigint('98765432')
    // x=12345678
    // q=x.\y  //  bigint division
    // 98765432/12345678
    // r=y-q*x 
    // pmodulo(98765432,12345678) //  division with doubles
    // y== q*x+r
    // r<abs(x)
    // (x*ones(2,2)).\y   // matrix case
    // 
    // See also
    //  mbigint
    // bigint_q_bigint
    // mbigint_q_bigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    q=x.\y
endfunction

