function [A]=%mbigint_sign(M)
    // overloading  compatibility  sign for mbigint
    // Calling Sequence
    //   A=sign(M)
    //
    // Parameters
    //  A,M: mbigint
    //
    // Description
    // mbigint sign : a matrix of +1,-1 or 0
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // y=bigint('-98765432') 
    // M=[x y 0]
    // sign(M)
    // 
    // See also
    //  sign
    //  mbigint
    //  mbigint_abs
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(M)
    A=M
    for i=1:p
        for j=1:n
           a=M.value{i,j} 
           A(i,j)=sign(a)
       end
   end
endfunction
