function reponse=millerrabin(n,varargin)
     // Miller-Rabin primality test for bigint
    // Calling Sequence
    //   reponse=milerrabin(x[,k])
    //
    // Parameters
    //  x: bigint
    //  k : integer
    //
    // Description
    //   return true if x passes consecutivelly k times  the Miller-Rabin primality test. Default value for k is 3.
    //
    // Examples
    // //
    //x=bigint(2)^17-1, //Mersenne  prime
    //millerrabin(x)
    //y=bigint(2)^23-1, //Mersenne not prime
    //millerrabin(y)
    //factorize(y)
    // 
    // See also
    //  factorize
    //
    // Authors
    //  Philippe Roux
    //
    [lhs,rhs]=argn(0)
    if rhs>1 then
        k=max(varargin(1),20)
        else k=3
    end
    n=bigint(n)
    d=n-1
    r=bigint(0)
    while odd(d)==0
        d=d/2
        r=r+1
    end
    i=0;
    //printf('n=%s =1+%s*2^%s\n',string(n),string(d),string(r))
    reponse=%T
    N=999999999
    if N>n-2 then N=evstr(string(n-2))
    end
    while (i<k)&reponse,
        a=bigint(grand(1,1,'uin',3,N))//int((n-4)*rand()+2) 
        //printf('tests avec a=%s\n',string(a))
        x=powermod(a,d,n)
        bool=(bmodulo(x-1,n)<>0)&(bmodulo(x+1,n)<>0)//  a^d<>1 mod n -> continuer
        //printf('%s^%s=%s mod %s\n',string(a),string(d),string(bmodulo(x,n)),string(n))
        s=1
        while (bool)&(s<r)
            s=s+1
            x=powermod(x,2,n)
            bool=(bmodulo(x+1,n)<>0)//  a^{2^s d}<>-1 mod n -> continuer
            //printf('%s^(2^%s %s)=%s mod %s\n',string(a),string(s),string(d),string(bmodulo(x,n)),string(n))
        end
        reponse=reponse&~((s==r)&(bool))
        i=i+1  
    end
endfunction


