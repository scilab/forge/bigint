function z=%bigint_p_bigint(x,y)
    // binary operator &#94; (power) for bigint
    // Calling Sequence
    //   z=x^y
    //
    // Parameters
    //  x,y,z: bigint
    //
    // Description
    // bigint power using fast exponentiation algorithm
    // <latex> x^{2k+1}= x\times (x^2)^k</latex>  or   <latex> x^{2k}= (x^2)^k</latex> 
    //
    // Examples
    // 
    // format('v',20)
    // x=bigint('5') 
    // y=bigint('7') 
    // x^7
    // 5^7  // compare with doubles
    // x=bigint('12345678') 
    // y=bigint('2') 
    // x^y
    // 12345678^2
    // 
    // See also
    //  bigint
    // div2
    // odd
    //
    // Authors
    //  Philippe Roux
    //

    if y==0 then //  x^0= 1
        z=bigint(1)
    elseif y.signe==-1 then // not integer !
        z=evstr(string(x)+'^('+string(y)+')') 
        warning('convert result from bigint to double!')
    elseif x.signe==-1 then // reduce to x,y>0
        z= (-1)^(odd(y))*((-x)^y)
    else //x,y>0  
        z=1
        while y>0
            if odd(y)==1 then 
                z=x*z
            end
            x=x*x
            y=div2(y)
        end
    end
    z=check_bigint(z)
endfunction
