function [b]=powermod(a,e,n)
    // bigint fast modular exponetiation
    // Calling Sequence
    //   [b]=powermod(a,e,n)
    //
    // Parameters
    //  x : bigint
    //
    // Description
    //  compute b=a^e mod n for bigint, intermediate results never exceed n^2.
    //
    // Examples
    // 
    // e=bigint(100)
    // b=2^e
    // powermod(2,e,100)
    // 
    // See also
    //  divide
    //  div2
    // odd
    //
    // Authors
    //  Philippe Roux
    //
     
    // convert to bigint if necessary
    if type(a)==1 then a=bigint(a)
    end
    if type(e)==1 then e=bigint(e)
    end
    b=bigint(1)
    u=a  
    while e<>0 
        if odd(e)<>0 then [q,b]=divide(b*u,n)
        end  
        [q,u]=divide(u*u,n)
        e=div2(e) 
    end
endfunction
