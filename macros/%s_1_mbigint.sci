function [bool]=%s_1_mbigint(x,A)
    // overloading compatibility for &#60; (lower) operator with double mbigint
    // Calling Sequence
    //   bool=(x<A)
    // Parameters
    //  A: mbigint
    //  x : double
    //  bool : boolean matrix
    //
    // Description
    // mbigint "lower" operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // 123456789<M
    // 2345678<M
    // 1234567890<M
    // //matrix case
    // [123456789 2345678; 1234567890 0]<M
    // 
    // See also
    //  mbigint
    // bigint_1_mbigint
    // mbigint_1_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    bool=(A>x)
endfunction
