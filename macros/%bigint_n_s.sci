function [bool]=%bigint_n_s(x,y)
    // overloading  compatibility  for operator &#60;&#62; (not equal) with bigint/double
    // Calling Sequence
    //       x<>y
    //   bool=(x<>y)
    //
    // Parameters
    // x : bigint
    // y : double(scalar)
    // bool : boolean
    //
    // Description
    // bigint/double "not equal" comparaison
    //
    // Examples
    //
    // x=bigint('123456789')
    // y=123456789
    // x<>y  //  false
    // y=12345678
    // x<>y // true
    // 
    // See also
    //  mbigint
    // bigint_n_bigint
    // bigint_n_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    y=mbigint(y)
    bool=(x<>y)
endfunction
