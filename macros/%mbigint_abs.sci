function [A]=%mbigint_abs(M)
    // overloading  compatibility  abs for mbigint
    // Calling Sequence
    //   A=abs(M)
    //
    // Parameters
    //  A,M: mbigint
    //
    // Description
    // bigint absolute value
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // y=bigint('-98765432') 
    // M=[x y]
    // abs(M)
    // 
    // See also
    //  abs
    //  mbigint
    //  mbigint_sign
    // abs
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(M)
    A=M
    for i=1:p
        for j=1:n
           a=M.value{i,j} 
           A(i,j)=abs(a)
       end
   end
endfunction
