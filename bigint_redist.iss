;##############################################################################################################
; Inno Setup Install script for bigint
; This file is released under licence CC-BY-SA-3.0 https://creativecommons.org/licenses/by-sa/3.0/fr/
;##############################################################################################################
; modify this path where is toolbox directory
#define BinariesSourcePath "C:\Programs files\scilab-5.0\contrib\bigint"
;
#define Toolbox_version "0.1"
#define CurrentYear "2017"
#define Toolbox_DirFilename "bigint"
;##############################################################################################################
[Setup]
; Debut Données de base à renseigner suivant version
SourceDir={#BinariesSourcePath}
AppName=bigint
AppVerName=big integers Macros version 0.1
DefaultDirName={pf}\{#Toolbox_DirFilename}
InfoAfterfile=readme.txt
LicenseFile=license.txt
WindowVisible=true
AppPublisher=Philippe Roux
BackColorDirection=lefttoright
AppCopyright=CC-BY-SA-3.0 © {#CurrentYear}
Compression=lzma/max
InternalCompressLevel=normal
SolidCompression=true
VersionInfoVersion={#Toolbox_version}
VersionInfoCompany=Philippe Roux Philippe.Roux@univ-rennes1.fr
;##############################################################################################################
[Files]
; Add here files that you want to add
Source: loader.sce; DestDir: {app}
;Source: builder.sce; DestDir: {app}
Source: changelog.txt; DestDir: {app}
Source: license.txt; DestDir: {app}
Source: readme.txt; DestDir: {app}
Source: etc\bigint.quit; DestDir: {app}\etc
Source: etc\bigint.start; DestDir: {app}\etc
;Source: macros\buildmacros.sce; DestDir: {app}\macros
;Source: macros\cleanmacros.sce; DestDir: {app}\macros
Source: macros\lib; DestDir: {app}\macros
Source: macros\names; DestDir: {app}\macros
;Source: macros\*.sci; DestDir: {app}\macros
Source: macros\*.bin; DestDir: {app}\macros
Source: help\*; DestDir: {app}\help; Flags: recursesubdirs
;Source: jar\*; DestDir: {app}\jar; Flags: recursesubdirs
;Source: tests\*; DestDir: {app}\tests; Flags: recursesubdirs
Source: demos\*; DestDir: {app}\locales; Flags: recursesubdirs
;
;##############################################################################################################
