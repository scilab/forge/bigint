mprintf('***************************************\n')
mprintf('** compilation of bigint toolbox     **\n')
mprintf('***************************************\n')

try
 out=getversion("scilab");if out(1)<6 then error(),end
catch
 error("Scilab 6.0 or more is required.");
end;
toolbox_title='bigint';
path=get_absolute_file_path("builder.sce")
//  compile macros
mprintf('***************************************\n')
mprintf('** compilation of macros/*.sci files **\n')
mprintf('***************************************\n')
cd(path+"macros")
exec("cleanmacros.sce")
exec("buildmacros.sce")
// generate  help  
mprintf('***************************************\n')
mprintf('** generating help files (xml/pdf)   **\n')
mprintf('***************************************\n')
cd("../help")
exec("builder_help.sce")
cd("../")
// xmltopdf('help/en_US',toolbox_title,'pdf');
//load
mprintf('***************************************\n')
mprintf('** loading  toolbox bigint           **\n')
mprintf('***************************************\n')
exec('etc/bigint.quit',-1)
exec('loader.sce',-1)
// run unit_tests
mprintf('***************************************\n')
mprintf('** running tests in test/*.tst files **\n')
mprintf('***************************************\n')
//status=test_run('.',[],'create_ref')// bugged????
fic=ls('tests/unit_tests/*tst');
liste_erreurs=[];
status=%t;
for cpt_fic=1:prod(size(fic))
    mprintf("%s\n",fic(cpt_fic))
    try exec(fic(cpt_fic),-1)
    catch status=%f;
        liste_erreurs($+1)=fic(cpt_fic);
        mprintf("%s<-error!!!!!!!!!\n",fic(cpt_fic))
        end
end
if status then
mprintf('***************************************\n')
mprintf('** successful compilation of bigint !**\n')
mprintf('** next time execute file loader.sce **\n')
mprintf('***************************************\n')
mprintf('***************************************\n')
mprintf('** read online help '"help bigint'"    **\n')
mprintf('***************************************\n')
help bigint
else
mprintf('***************************************\n')
mprintf('**/!\\ errors detected! check tests/!\\**\n')
mprintf('***************************************\n')
disp(liste_erreurs)
mprintf('***************************************\n')
mprintf('** next time execute file loader.sce **\n')
mprintf('***************************************\n')

end
