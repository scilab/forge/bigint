readme.txt of bigint
=========================

* provide macros for calculations with big integers (>10^15)  to avoid rounding/truncature erros
* Author Philippe Roux, mail : Philippe.Roux@univ-rennes1.fr
* licence CC-BY-SA-3.0 https://creativecommons.org/licenses/by-sa/3.0/fr/
* web site : http://forge.scilab.org/index.php/p/bigint/
* install instructions unzip bigint.zip and exec('load.sce',-1) 
